/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uz.projekt.auth;

/**
 *
 * @author Domon
 */
public class AuthClass {
    public boolean logged=false;
    public Integer authLevel=0;
    
    public Integer getAuthLevel(){
        return this.authLevel;
    }
    
    public String getAuthLevelString(){
        return Integer.toString(this.authLevel);
    }
    
    public AuthClass() {
        logged = false;
        authLevel = 0;
    }
    public void setLoggedAsUser(){
        this.logged = true;
        this.authLevel = 1;
    }
    public void setLoggedAsAdmin(){
        this.logged = true;
        this.authLevel = 2;
    }
    public void logout(){
        this.logged = false;
        this.authLevel = 0;
    }
   

}
