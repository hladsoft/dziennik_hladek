/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uz.projekt.db;

/**
 *
 * @author Rain
 */
public class Przedmiot {

    private int id;
    private int idWykladowcy;
    private String nazwa;

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the idWykladowcy
     */
    public int getIdWykladowcy() {
        return idWykladowcy;
    }

    /**
     * @param idWykladowcy the idWykladowcy to set
     */
    public void setIdWykladowcy(int idWykladowcy) {
        this.idWykladowcy = idWykladowcy;
    }

    /**
     * @return the nazwa
     */
    public String getNazwa() {
        return nazwa;
    }

    /**
     * @param nazwa the nazwa to set
     */
    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }
}
