/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uz.projekt.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import uz.projekt.exceptions.anyException;

/**
 *
 * @author Rain
 */
public class ConnectionManager {

    protected static Connection conn = null;
    protected static Statement stmt = null;
    protected final static String DBURL = "jdbc:mysql://localhost:3306/test"; //daj swoje dane!
    protected final static String DBUSER = "root"; //up
    protected final static String DBPASS = "haslo"; //up
    protected final static String DBDRIVER = "com.mysql.jdbc.Driver"; //pamietaj o dodaniu biblioteki !
    SqlParser sqlParser;

    public ConnectionManager() {
        sqlParser = new SqlParser();
    }

    public void createConnection() {
        try {
            Class.forName(DBDRIVER).newInstance();
            conn = DriverManager.getConnection(DBURL, DBUSER, DBPASS);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            //throw new ProjectConnectionException(ex.getMessage());           
        }
    }

    public List selectStudents() {
        String query = sqlParser.createReadAllQuery();  //checkme
        Student student;
        List studentList = new ArrayList();
        try {
            stmt = conn.createStatement();
            ResultSet results = stmt.executeQuery(query);
            while (results.next()) {
                student = new Student();
                student.setId(results.getInt("id_student"));
                student.setIdGrupa(results.getInt("id_grupa"));
                student.setImie(results.getString("imie"));
                student.setNazwisko(results.getString("nazwisko"));
                student.setPesel(results.getLong("pesel"));
                student.setNrIndeksu(results.getInt("nr_indeksu")); //KURWA MAC
                student.setRok(results.getInt("rok"));
                studentList.add(student);
            }

        } catch (SQLException sqlExcept) {
            sqlExcept.printStackTrace();
        }
        return studentList;
    }

    public List selectWykladowcy() {
        String query = sqlParser.createReadAllWykladowcy();  //checkme
        Wykladowca wykladowca;
        List wykladowcyList = new ArrayList();
        try {
            stmt = conn.createStatement();
            ResultSet results = stmt.executeQuery(query);
            while (results.next()) {
                wykladowca = new Wykladowca();
                wykladowca.setIdWykladowca(results.getInt("id_wykladowca"));
                wykladowca.setNazwisko(results.getString("nazwisko"));
                wykladowca.setImie(results.getString("imie"));
                //wykladowca.setIdLogin(results.getInt(4));
                wykladowcyList.add(wykladowca);
            }
        } catch (SQLException sqlExcept) {
            sqlExcept.printStackTrace();
        }
        return wykladowcyList;
    }

    public List selectKonto() {
        String query = sqlParser.createReadAllKonta();  //checkme
        Konto konto;
        List kontaList = new ArrayList();
        try {
            stmt = conn.createStatement();
            ResultSet results = stmt.executeQuery(query);
            while (results.next()) {
                konto = new Konto();
                konto.setLogin(results.getString("login"));
                konto.setPassword(results.getString("haslo"));
                konto.setIsAdmin(results.getByte("is_admin"));
                kontaList.add(konto);
            }
        } catch (SQLException sqlExcept) {
            sqlExcept.printStackTrace();
        }
        return kontaList;
    }

    public List selectGrupa() {
        String query = sqlParser.createReadAllGrupy();  //checkme
        Grupa grupa;
        List grupaList = new ArrayList();
        try {
            stmt = conn.createStatement();
            ResultSet results = stmt.executeQuery(query);
            while (results.next()) {
                grupa = new Grupa();
                grupa.setId(results.getInt("id_grupa"));
                grupa.setNazwa(results.getString("nazwa"));
                grupaList.add(grupa);
            }
        } catch (SQLException sqlExcept) {
            sqlExcept.printStackTrace();
        }
        return grupaList;
    }

    public List selectPrzedmiot() {
        String query = sqlParser.createReadAllPrzedmioty();  //checkme
        Przedmiot przedmiot;
        List przedmiotList = new ArrayList();
        try {
            stmt = conn.createStatement();
            ResultSet results = stmt.executeQuery(query);
            while (results.next()) {
                przedmiot = new Przedmiot();
                przedmiot.setId(results.getInt("id_przedmiot"));
                przedmiot.setNazwa(results.getString("nazwa"));
                przedmiot.setIdWykladowcy(results.getInt("id_wykladowca"));
                przedmiotList.add(przedmiot);
            }
        } catch (SQLException sqlExcept) {
            sqlExcept.printStackTrace();
        }
        return przedmiotList;
    }

    public List selectOcena() {
        String query = sqlParser.createReadAllOceny();  //checkme
        Ocena ocena;
        List ocenaList = new ArrayList();
        try {
            stmt = conn.createStatement();
            ResultSet results = stmt.executeQuery(query);
            while (results.next()) {
                ocena = new Ocena();
                ocena.setId_ocena(results.getInt("id_ocena"));
                ocena.setOcena(results.getInt("ocena"));
                ocena.setStudent_id(results.getInt("id_student"));
                ocena.setId_przedmiotu(results.getInt("id_przedmiot"));
                ocenaList.add(ocena);
            }
        } catch (SQLException sqlExcept) {
            sqlExcept.printStackTrace();
        }
        return ocenaList;
    }

    public List selectOcena(StudentPrzedmiot stuprzed) {
        String query = sqlParser.createReadFewOceny(stuprzed);  //checkme
        Ocena ocena;
        List ocenaList = new ArrayList();
        try {
            stmt = conn.createStatement();
            ResultSet results = stmt.executeQuery(query);
            while (results.next()) {
                ocena = new Ocena();
                ocena.setId_ocena(results.getInt("id_ocena"));
                ocena.setOcena(results.getFloat("ocena"));
                ocena.setStudent_id(results.getInt("id_student"));
                ocena.setId_przedmiotu(results.getInt("id_przedmiot"));
                ocenaList.add(ocena);
            }
        } catch (SQLException sqlExcept) {
            sqlExcept.printStackTrace();
        }
        return ocenaList;
    }

    public <T> void addT(T object) throws anyException{
        String query = "";
        if (object instanceof Konto) {
            query = sqlParser.createAddQuery((Konto) object);
        }
        if (object instanceof Student) {
            query = sqlParser.createAddQuery((Student) object);
        }
        if (object instanceof Wykladowca) {
            query = sqlParser.createAddQuery((Wykladowca) object);
        }
        if (object instanceof Grupa) {
            query = sqlParser.createAddQuery((Grupa) object);
        }
        if (object instanceof Przedmiot) {
            query = sqlParser.createAddQuery((Przedmiot) object);
        }
        if (object instanceof Ocena) {
            query = sqlParser.createAddQuery((Ocena) object);
        }
        try {
            stmt = conn.createStatement();
            stmt.executeUpdate(query);
            stmt.close();
        } catch (SQLException err) {
            System.out.println(err.getMessage());
            throw new anyException();
        }
    }

    public <T> void deleteT(T object) throws anyException{
        String query = "";
        if (object instanceof Konto) {
            query = sqlParser.createDeleteQuery((Konto) object);
        }
        if (object instanceof Student) {
            query = sqlParser.createDeleteQuery((Student) object);
        }
        if (object instanceof Wykladowca) {
            query = sqlParser.createDeleteQuery((Wykladowca) object);
        }
        if (object instanceof Grupa) {
            query = sqlParser.createDeleteQuery((Grupa) object);
        }
        if (object instanceof Przedmiot) {
            query = sqlParser.createDeleteQuery((Przedmiot) object);
        }
        if (object instanceof Ocena) {
            query = sqlParser.createDeleteQuery((Ocena) object);
        }
        try {
            stmt = conn.createStatement();
            stmt.executeUpdate(query);
            stmt.close();
        } catch (SQLException err) {
            System.out.println(err.getMessage());
            throw new anyException();
        }
    }

    public <T> void editT(T object) throws anyException{
        String query = "";
        if (object instanceof Konto) {
            query = sqlParser.createUpdateQuery((Konto) object);
        }
        if (object instanceof Student) {
            query = sqlParser.createUpdateQuery((Student) object);
        }
        if (object instanceof Wykladowca) {
            query = sqlParser.createUpdateQuery((Wykladowca) object);
        }
        if (object instanceof Grupa) {
            query = sqlParser.createUpdateQuery((Grupa) object);
        }
        if (object instanceof Przedmiot) {
            query = sqlParser.createUpdateQuery((Przedmiot) object);
        }
        if (object instanceof Ocena) {
            query = sqlParser.createUpdateQuery((Ocena) object);
        }
        try {
            stmt = conn.createStatement();
            stmt.executeUpdate(query);
            stmt.close();
        } catch (SQLException err) {
            System.out.println(err.getMessage());
            throw new anyException();
        }
    }

    public <T> void readOneT(T object) {
        String query = "";
        if (object instanceof Konto) {
            Konto konto = (Konto) object;
            query = sqlParser.createReadOneQuery(konto);
            try {
                stmt = conn.createStatement();
                ResultSet results = stmt.executeQuery(query);
                results.next();
                konto.setLogin(results.getString("login"));
                konto.setPassword(results.getString("haslo"));
                konto.setIsAdmin(results.getByte("is_admin"));
                results.close();
            } catch (SQLException err) {
                System.out.println(err.getMessage());
            }
        }
        if (object instanceof Student) {
            Student student = (Student) object;
            query = sqlParser.createReadOneQuery(student);
            try {
                stmt = conn.createStatement();
                ResultSet results = stmt.executeQuery(query);
                results.next();
                student.setIdGrupa(results.getInt("id_grupa"));
                student.setImie(results.getString("imie"));
                student.setNazwisko(results.getString("nazwisko"));
                student.setPesel(results.getLong("pesel"));
                student.setNrIndeksu(results.getInt("nr_indeksu"));
                student.setRok(results.getInt("rok"));
                results.close();
            } catch (SQLException err) {
                System.out.println(err.getMessage());;
            }
        }
        if (object instanceof Wykladowca) {
            Wykladowca wykladowca = (Wykladowca) object;
            query = sqlParser.createReadOneQuery(wykladowca);
            try {
                stmt = conn.createStatement();
                ResultSet results = stmt.executeQuery(query);
                results.next();
                wykladowca.setNazwisko(results.getString("nazwisko"));
                wykladowca.setImie(results.getString("imie"));
                //wykladowca.setIdLogin(results.getInt(4));
                results.close();
            } catch (SQLException err) {
                System.out.println(err.getMessage());;
            }
        }
        if (object instanceof Grupa) {
            Grupa grupa = (Grupa) object;
            query = sqlParser.createReadOneQuery(grupa);
            try {
                stmt = conn.createStatement();
                ResultSet results = stmt.executeQuery(query);
                results.next();
                grupa.setNazwa(results.getString("nazwa"));
                results.close();
            } catch (SQLException err) {
                System.out.println(err.getMessage());;
            }
        }
        if (object instanceof Przedmiot) {
            Przedmiot przedmiot = (Przedmiot) object;
            query = sqlParser.createReadOneQuery(przedmiot);
            try {
                stmt = conn.createStatement();
                ResultSet results = stmt.executeQuery(query);
                results.next();
                przedmiot.setNazwa(results.getString("nazwa"));
                przedmiot.setIdWykladowcy(results.getInt("id_wykladowca"));
                results.close();
            } catch (SQLException err) {
                System.out.println(err.getMessage());;
            }
        }
        if (object instanceof Ocena) {
            Ocena ocena = (Ocena) object;
            query = sqlParser.createReadOneQuery(ocena);
            try {
                stmt = conn.createStatement();
                ResultSet results = stmt.executeQuery(query);
                results.next();
                ocena.setOcena(results.getFloat("ocena"));
                results.close();
            } catch (SQLException err) {
                System.out.println(err.getMessage());;
            }
        }
    }

    public void closeConnection() {
        try {
            if (stmt != null) {
                stmt.close();
            }
            if (conn != null) {
                DriverManager.getConnection(DBURL, DBUSER, DBPASS + ";shutdown=true");
                conn.close();
            }
        } catch (SQLException sqlExcept) {
        }

    }

    public String readPass(Konto user) { //TODO poprawic uprawnienia! filip dodaj boolean do konto o nazwie ADMIN: bedzie definiowac czy konto jest adminem czy nie, po dodaniu NAPRAWIC LGOOWANIE tu i w WebPages/logowanie
        String query = "SELECT haslo FROM konto WHERE login='" + user.getLogin() + "';";
        String pass = null;
        try {
            stmt = conn.createStatement();
            ResultSet results = stmt.executeQuery(query);
            results.next();
            pass = results.getString("haslo");
            results.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            System.out.println("error");
            e.printStackTrace();
        }
        return pass;
    }

    public byte readIsAdmin(Konto user) {
        String query = "SELECT is_admin FROM konto WHERE login='" + user.getLogin() + "';";
        byte isAdmin = -1;
        try {
            stmt = conn.createStatement();
            ResultSet results = stmt.executeQuery(query);
            results.next();
            isAdmin = results.getByte("is_admin");
            results.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            System.out.println("error");
            e.printStackTrace();
        }
        return isAdmin;
    }

    public boolean userExists(Konto user) {
        String passwordFromSource = readPass(user);
        if (passwordFromSource != null) {
            return user.getPassword().equals(passwordFromSource);
        } else {
            return false;
        }
    }

    public boolean userIsAdmin(Konto user) {
        if (this.readIsAdmin(user) == 1) {
            return true;
        } else {
            return false;
        }
    }

    public String getNameGroup(Integer i) {
        String match = null;
        String query = "SELECT nazwa FROM grupa WHERE id_grupa=" + i + ";";
        try {
            stmt = conn.createStatement();
            ResultSet results = stmt.executeQuery(query);
            if (results.next()) {
                match = results.getString("nazwa");
            }
        } catch (SQLException sqlExcept) {
            sqlExcept.printStackTrace();
        }
        return match;
    }

    public String getNameWykladowca(Integer i) {
        String match = null;
        String query = "SELECT `imie`, `nazwisko` FROM `wykladowca` WHERE `id_wykladowca` = " + i + ";";
        try {
            stmt = conn.createStatement();
            ResultSet results = stmt.executeQuery(query);
            if (results.next()) {
                match = results.getString("nazwisko") + " " + results.getString("imie");
            }
        } catch (SQLException sqlExcept) {
            sqlExcept.printStackTrace();
        }
        return match;
    }

    public String getNamePrzedmiot(Integer i) {
        String match = null;
        String query = "SELECT  nazwa FROM przedmiot WHERE id_przedmiot = " + i + ";";
        try {
            stmt = conn.createStatement();
            ResultSet results = stmt.executeQuery(query);
            if (results.next()) {
                match = results.getString("nazwa");
            }
        } catch (SQLException sqlExcept) {
            sqlExcept.printStackTrace();
        }
        return match;
    }

}
