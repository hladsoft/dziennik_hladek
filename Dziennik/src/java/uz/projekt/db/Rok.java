/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uz.projekt.db;

/**
 *
 * @author Rain
 */
public class Rok {

    private int id;
    private String rok;

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the rok
     */
    public String getRok() {
        return rok;
    }

    /**
     * @param rok the rok to set
     */
    public void setRok(String rok) {
        this.rok = rok;
    }
}
