/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uz.projekt.db;

/**
 *
 * @author Rain
 */
public class Ocena {

    private float ocena;
    private int id_przedmiotu;
    private int student_id;
    private int id_ocena;

    /**
     * @return the id_przedmiotu
     */
    public int getId_przedmiotu() {
        return id_przedmiotu;
    }

    /**
     * @param id_przedmiotu the id_przedmiotu to set
     */
    public void setId_przedmiotu(int id_przedmiotu) {
        this.id_przedmiotu = id_przedmiotu;
    }

    /**
     * @return the student_id
     */
    public int getStudent_id() {
        return student_id;
    }

    /**
     * @param student_id the student_id to set
     */
    public void setStudent_id(int student_id) {
        this.student_id = student_id;
    }


    /**
     * @return the ocena
     */
    public float getOcena() {
        return ocena;
    }

    /**
     * @param ocena the ocena to set
     */
    public void setOcena(float ocena) {
        this.ocena = ocena;
    }

    /**
     * @return the id_ocena
     */
    public int getId_ocena() {
        return id_ocena;
    }

    /**
     * @param id_ocena the id_ocena to set
     */
    public void setId_ocena(int id_ocena) {
        this.id_ocena = id_ocena;
    }
    
}
