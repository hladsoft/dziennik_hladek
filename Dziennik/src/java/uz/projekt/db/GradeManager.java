/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uz.projekt.db;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 *
 * @author Rain
 */
public class GradeManager extends ConnectionManager { //klasa stworzona na potrzeby komunikacji z baza zeby dostac oceny, podobny trik mozna zrobic dla kazdej z tabel, i podzielic managery (jesli jest sens?)

    private HashMap<Integer, ArrayList<Float>> gradeMap;
    private HashMap<Integer, String> nameMap;
    private HashMap<Integer, Float> finalGradeMap;
    //Set<Integer> przedmioty;
    // private Iterator<Integer> iteratorPrzedmioty; //checkme

    public GradeManager() {
        gradeMap = new HashMap();
        nameMap = new HashMap();
        finalGradeMap= new HashMap();
    }

    public Iterator<Integer> selectClasses(Student student) {
        String query = "SELECT DISTINCT id_przedmiot FROM ocena WHERE id_student=" + student.getId() + ";";  //checkme
        //Student student;
        Set<Integer> classes = new HashSet();
        try {
            stmt = conn.createStatement();
            ResultSet results = stmt.executeQuery(query);
            while (results.next()) {

                classes.add(results.getInt("id_przedmiot"));
            }

        } catch (SQLException sqlExcept) {
            sqlExcept.printStackTrace();
        }
        //przedmioty=classes;

        return classes.iterator();
    }

    public Iterator<Integer> selectClasses(Przedmiot przedmiot) { //mylaca nazwa, nie wybiera typow zajec, tylko studentow chodzacych na dane zajecia
        String query = "SELECT DISTINCT id_student FROM ocena WHERE id_przedmiot=" + przedmiot.getId() + ";";  //checkme
        //Student student;
        Set<Integer> classes = new HashSet();
        try {
            stmt = conn.createStatement();
            ResultSet results = stmt.executeQuery(query);
            while (results.next()) {

                classes.add(results.getInt("id_student"));
            }

        } catch (SQLException sqlExcept) {
            sqlExcept.printStackTrace();
        }
        //przedmioty=classes;

        return classes.iterator();
    }

    public Iterator<String> getClassesNames(Set<Integer> setOfId) {
        Iterator<Integer> iter = setOfId.iterator();
        Integer i;
        String nam;
        Set<String> returnedSet = new HashSet();
        while (iter.hasNext()) {
            i = iter.next();
            String query = "SELECT nazwa FROM przedmiot WHERE id_przedmiot=" + i + ";";
            try {
                stmt = conn.createStatement();
                ResultSet results = stmt.executeQuery(query);
                while (results.next()) {

                    returnedSet.add(results.getString("nazwa"));
                }

            } catch (SQLException sqlExcept) {
                sqlExcept.printStackTrace();
            }
        }
        return returnedSet.iterator();
    }

    public void selectGrades(Student student) {
        //Set<Integer> nazwy= selectClasses(student);
        Iterator<Integer> przedmioty = selectClasses(student);
        // iteratorPrzedmioty=przedmioty;
        //System.out.println("wchodze do petlo");
        Integer przedmiot;

        while (przedmioty.hasNext()) {
            przedmiot = przedmioty.next();
            ArrayList<Float> oceny = new ArrayList();
            String query = "SELECT ocena FROM ocena WHERE id_przedmiot='" + przedmiot + "' AND id_student=" + student.getId() + ";";
            try {
                stmt = conn.createStatement();
                ResultSet results = stmt.executeQuery(query);
                while (results.next()) {
                    oceny.add(results.getFloat("ocena"));
                }
                results.close();
                gradeMap.put(przedmiot, oceny);

            } catch (SQLException sqlExcept) {
                sqlExcept.printStackTrace();
            }
        }
    }

    public void selectGrades(Przedmiot przedmiot) {
        //Set<Integer> nazwy= selectClasses(student);
        Iterator<Integer> studenci = selectClasses(przedmiot);
        // iteratorPrzedmioty=przedmioty;
        //System.out.println("wchodze do petlo");
        Integer student;

        while (studenci.hasNext()) {
            student = studenci.next();
            ArrayList<Float> oceny = new ArrayList();
            String query = "SELECT ocena FROM ocena WHERE id_przedmiot=" + przedmiot.getId() + " AND id_student=" + student + ";";
            try {
                stmt = conn.createStatement();
                ResultSet results = stmt.executeQuery(query);
                while (results.next()) {
                    oceny.add(results.getFloat("ocena"));
                }
                results.close();
                gradeMap.put(student, oceny);

            } catch (SQLException sqlExcept) {
                sqlExcept.printStackTrace();
            }
        }
    }

    public String matchNames(Integer i) {
        String match = null;
        String query = "SELECT nazwa FROM przedmiot WHERE id_przedmiot=" + i + ";";
        try {
            stmt = conn.createStatement();
            ResultSet results = stmt.executeQuery(query);
            while (results.next()) {

                nameMap.put(i, results.getString("nazwa"));

            }

        } catch (SQLException sqlExcept) {
            sqlExcept.printStackTrace();
        }
        match = nameMap.get(i);
        return match;
    }

    public String matchNamesStudent(Integer i) {
        String match = null;
        String query = "SELECT imie, nazwisko FROM student WHERE id_student=" + i + ";";
        try {
            stmt = conn.createStatement();
            ResultSet results = stmt.executeQuery(query);
            while (results.next()) {
                nameMap.put(i, results.getString("imie") + " " + results.getString("nazwisko"));

            }

        } catch (SQLException sqlExcept) {
            sqlExcept.printStackTrace();
        }
        match = nameMap.get(i);
        return match;
    }

    /**
     * @return the iteratorPrzedmioty
     */
    //public Iterator<Integer> getIteratorPrzedmioty() {
    //return iteratorPrzedmioty;
    // }
    /**
     * @return the gradeMap
     */
    public HashMap<Integer, ArrayList<Float>> getGradeMap() {
        return gradeMap;
    }

    public float getAverageOfStudent() { //funkcja wykonwyana do raportu, wyrzuca srednia ocen studenta ze wszystkich ocenionych przedmiotow 
        float avg = (float) 0.0; //to change
        Set<Integer> przedmioty = gradeMap.keySet();
        Object array[] = przedmioty.toArray();
        int i = 0;

        for (Object obj : array) {

            ArrayList<Float> sredniaPrzedmiot = gradeMap.get(obj);
            i++;
            int iloscOcen = sredniaPrzedmiot.size();
            Float avgOne = 0.0f;
            Iterator<Float> iter = sredniaPrzedmiot.iterator();
            while (iter.hasNext()) {
                Float tmp = iter.next();
                avgOne = avgOne + tmp;
            }
            avgOne = avgOne / iloscOcen;
            avg = avg + avgOne;
        }
        avg = avg / i;
        return avg;
    }
    
        public void ocenyKoncowe(Student student){
       // Set<Integer> setOfIds=gradeMap.keySet();
        String query = "SELECT ocenaKoncowa, id_przedmiot FROM ocenakoncowa WHERE id_student="+student.getId()+";";
                try {
            stmt = conn.createStatement();
            ResultSet results = stmt.executeQuery(query);
            while (results.next()) {

                finalGradeMap.put(results.getInt("id_przedmiot"), results.getFloat("ocenaKoncowa"));

            }

        } catch (SQLException sqlExcept) {
            sqlExcept.printStackTrace();
        }
        
    }
    
        public void ocenyKoncowe(Przedmiot przedmiot){
       // Set<Integer> setOfIds=gradeMap.keySet();
        String query = "SELECT ocenaKoncowa, id_student FROM ocenakoncowa WHERE id_przedmiot="+przedmiot.getId()+";";
                try {
            stmt = conn.createStatement();
            ResultSet results = stmt.executeQuery(query);
            while (results.next()) {

                finalGradeMap.put(results.getInt("id_student"), results.getFloat("ocenaKoncowa"));

            }

        } catch (SQLException sqlExcept) {
            sqlExcept.printStackTrace();
        }
        
    }
        
        public void pushOcenaK(OcenaKoncowa ocena){
             String query ="REPLACE INTO ocenaKoncowa (ocenaKoncowa, id_student, id_przedmiot) VALUES("+ocena.getOcenaKoncowa()+","+ocena.getId_student()+","+ocena.getId_przedmiot()+");";
             try {
            stmt = conn.createStatement();
            stmt.executeUpdate(query);
            stmt.close();
        } catch (SQLException err) {
            System.out.println(err.getMessage());
        }
        }



    /**
     * @return the finalGradeMap
     */
    public HashMap<Integer, Float> getFinalGradeMap() {
        return finalGradeMap;
    }
}
