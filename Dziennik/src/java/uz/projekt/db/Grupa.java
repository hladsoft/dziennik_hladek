/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uz.projekt.db;

/**
 *
 * @author Rain
 */
public class Grupa {

    private int id;
//private int idRok;
    private String nazwa;

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the idRok
     */
    //  public int getIdRok() {
    //   return idRok;
    // }
    /**
     * @param idRok the idRok to set
     */
    // public void setIdRok(int idRok) {
    //   this.idRok = idRok;
    // }
    /**
     * @return the nazwa
     */
    public String getNazwa() {
        return nazwa;
    }

    /**
     * @param nazwa the nazwa to set
     */
    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }
}
