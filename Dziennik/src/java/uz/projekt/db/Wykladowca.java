/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uz.projekt.db;

/**
 *
 * @author Rain
 */
public class Wykladowca {

    private int idWykladowca;
    private String imie;
    private String nazwisko;
    // private int idLogin;

    /**
     * @return the idWykladowca
     */
    public int getIdWykladowca() {
        return idWykladowca;
    }

    /**
     * @param idWykladowca the idWykladowca to set
     */
    public void setIdWykladowca(int idWykladowca) {
        this.idWykladowca = idWykladowca;
    }

    /**
     * @return the imie
     */
    public String getImie() {
        return imie;
    }

    /**
     * @param imie the imie to set
     */
    public void setImie(String imie) {
        this.imie = imie;
    }

    /**
     * @return the nazwisko
     */
    public String getNazwisko() {
        return nazwisko;
    }

    /**
     * @param nazwisko the nazwisko to set
     */
    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    /* 
    public int getIdLogin() {
        return idLogin;
    }

    
    public void setIdLogin(int idLogin) {
        this.idLogin = idLogin;
    } */
}
