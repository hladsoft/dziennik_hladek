/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uz.projekt.db;

/**
 *
 * @author Rain
 */
public class SqlParser /*implements SqlParserInterface*/ { //IMPLEMENTUJ METODY INTEFESJU

    public String createAddQuery(Student student) {
        String query = "";
        query = "INSERT INTO `student`(`id_student`, `imie`, `nazwisko`, `pesel`, `nr_indeksu`, `id_grupa`, `rok`) "
                + "VALUES (NULL,'" + student.getImie() + "', '" + student.getNazwisko() + "', " + student.getPesel()
                + ", " + student.getNrIndeksu() + ", " + student.getIdGrupa() + ", " + student.getRok() + ");";
        return query;
    }

    public String createDeleteQuery(Student student) {
        String query = "";
        query = "DELETE FROM Student WHERE id_student=" + student.getId() + ";";
        return query;
    }

    public String createUpdateQuery(Student student) {
        String query = "";
        query = "UPDATE student SET imie= '" + student.getImie() + "',nazwisko='" + student.getNazwisko()
                + "',pesel=" + student.getPesel() + ",nr_indeksu=" + student.getNrIndeksu()
                + ",id_grupa=" + student.getIdGrupa() + ",rok="
                + student.getRok() + " WHERE id_student = " + student.getId() + ";";
        return query;
    }

    public String createReadOneQuery(Student student) {
        String query = "";
        query = "SELECT * FROM student WHERE id_student=" + student.getId() + ";";
        return query;
    }

    public String createReadAllQuery() {
        String query = "";
        query = "SELECT * FROM student;";
        return query;
    }

    //Wykladowcy
    public String createAddQuery(Wykladowca wykladowca) {
        String query = "";
        query = "INSERT INTO wykladowca (id_wykladowca, nazwisko, imie) VALUES (NULL,'" + wykladowca.getNazwisko()
                + "','" + wykladowca.getImie() + "');";
        return query;
    }

    public String createDeleteQuery(Wykladowca wykladowca) {
        String query = "";
        query = "DELETE FROM wykladowca WHERE id_wykladowca=" + wykladowca.getIdWykladowca() + ";";
        return query;
    }

    String createUpdateQuery(Wykladowca wykladowca) {
        String query = "";
        query = "UPDATE wykladowca SET Nazwisko='" + wykladowca.getNazwisko()
                + "', Imie='" + wykladowca.getImie() + "' WHERE id_wykladowca=" + wykladowca.getIdWykladowca() + ";";
        return query;
    }

    String createReadOneQuery(Wykladowca wykladowca) {
        String query = "";
        query = "SELECT * FROM wykladowca WHERE id_wykladowca=" + wykladowca.getIdWykladowca();
        return query;
    }

    String createReadAllWykladowcy() {
        String query = "";
        query = "SELECT * FROM wykladowca";
        return query;
    }

    //Konta
    public String createAddQuery(Konto konto) {
        String query = "";
        query = "INSERT INTO konto(`login`, `haslo`, `is_admin`) VALUES ('" + konto.getLogin()
                + "','" + konto.getPassword() + "'," + konto.getIsAdmin() + ");";
        return query;
    }

    public String createDeleteQuery(Konto konto) {
        String query = "";
        query = "DELETE FROM konto WHERE login='" + konto.getLogin() + "';";
        return query;
    }

    String createUpdateQuery(Konto konto) {
        String query = "";
        query = "UPDATE konto SET haslo='" + konto.getPassword()
                + "', is_admin = " + konto.getIsAdmin() + " WHERE login='" + konto.getLogin() + "';";
        return query;
    }

    String createReadOneQuery(Konto konto) {
        String query = "";
        query = "SELECT * FROM konto WHERE login='" + konto.getLogin() + "';";
        return query;
    }

    String createReadAllKonta() {
        String query = "";
        query = "SELECT * FROM konto";
        return query;
    }
    //Grupa

    public String createAddQuery(Grupa grupa) {
        String query = "";
        query = "INSERT INTO `grupa`(`id_grupa`, `nazwa`) VALUES (NULL,'" + grupa.getNazwa() + "')";
        return query;
    }

    public String createDeleteQuery(Grupa grupa) {
        String query = "";
        query = "DELETE FROM grupa WHERE id_grupa='" + grupa.getId() + "';";
        return query;
    }

    String createUpdateQuery(Grupa grupa) {
        String query = "";
        query = "UPDATE grupa SET nazwa='" + grupa.getNazwa() + "' WHERE `id_grupa` = " + grupa.getId() + ";";
        return query;
    }

    String createReadOneQuery(Grupa grupa) {
        String query = "";
        query = "SELECT * FROM grupa WHERE id_grupa=" + grupa.getId() + ";";
        return query;
    }

    String createReadAllGrupy() {
        String query = "";
        query = "SELECT * FROM grupa";
        return query;
    }

    //Przedmioty
    public String createAddQuery(Przedmiot przedmiot) {
        String query = "";
        query = "INSERT INTO `przedmiot`(`id_przedmiot`, `nazwa`, `id_wykladowca`) VALUES (NULL, '" + przedmiot.getNazwa() + "', " + przedmiot.getIdWykladowcy() + ");";
        return query;
    }

    public String createDeleteQuery(Przedmiot przedmiot) {
        String query = "";
        query = "DELETE FROM przedmiot WHERE id_przedmiot=" + przedmiot.getId() + ";";
        return query;
    }

    String createUpdateQuery(Przedmiot przedmiot) {
        String query = "";
        query = "UPDATE przedmiot SET nazwa='" + przedmiot.getNazwa() + "', id_wykladowca=" + przedmiot.getIdWykladowcy() + " WHERE `id_przedmiot` = " + przedmiot.getId() + ";";
        return query;
    }

    String createReadOneQuery(Przedmiot przedmiot) {
        String query = "";
        query = "SELECT * FROM przedmiot WHERE id_przedmiot=" + przedmiot.getId() + ";";
        return query;
    }

    String createReadAllPrzedmioty() {
        String query = "";
        query = "SELECT * FROM przedmiot";
        return query;
    }

    //Oceny
    public String createAddQuery(Ocena ocena) {
        String query = "";
        query = "INSERT INTO `ocena`(`id_ocena`, `ocena`, `id_student`, `id_przedmiot`) VALUES (NULL, " + ocena.getOcena() + ", " + ocena.getStudent_id() + ", " + ocena.getId_przedmiotu() + ");";
        return query;
    }

    public String createDeleteQuery(Ocena ocena) {
        String query = "";
        query = "DELETE FROM `ocena` WHERE `ocena`.`id_ocena` = " + ocena.getId_ocena() + ";";
        return query;
    }

    String createUpdateQuery(Ocena ocena) {
        String query = "";
        query = "UPDATE `ocena` SET `ocena`= " + ocena.getOcena() + " WHERE id_ocena=" + ocena.getId_ocena() + ";";
        return query;
    }

    String createReadOneQuery(Ocena ocena) {
        String query = "";
        query = "SELECT * FROM ocena WHERE id_ocena=" + ocena.getId_ocena() + ";";
        return query;
    }

    String createReadAllOceny() {
        String query = "";
        query = "SELECT * FROM ocena";
        return query;
    }

    String createReadFewOceny(StudentPrzedmiot studentPrzedmiot) {
        String query = "";
        query = "SELECT * FROM ocena WHERE id_student=" + studentPrzedmiot.getStudent_id() + " AND id_przedmiot=" + studentPrzedmiot.getId_przedmiotu() + ";";
        return query;
    }
}
