/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uz.projekt.db;

/**
 *
 * @author Rain
 */
public class OcenaKoncowa {
   // private int id_OcenaKoncowa;
    private float ocenaKoncowa;
    private int id_przedmiot;
    private int id_student;

    /**
     * @return the id_OcenaKoncowa
     */
  //  public int getId_OcenaKoncowa() {
  //      return id_OcenaKoncowa;
  //  }

    /**
     * @param id_OcenaKoncowa the id_OcenaKoncowa to set
     */
   // public void setId_OcenaKoncowa(int id_OcenaKoncowa) {
   //     this.id_OcenaKoncowa = id_OcenaKoncowa;
  //  }

    /**
     * @return the ocenaKoncowa
     */
    public float getOcenaKoncowa() {
        return ocenaKoncowa;
    }

    /**
     * @param ocenaKoncowa the ocenaKoncowa to set
     */
    public void setOcenaKoncowa(float ocenaKoncowa) {
        this.ocenaKoncowa = ocenaKoncowa;
    }

    /**
     * @return the id_przedmiot
     */
    public int getId_przedmiot() {
        return id_przedmiot;
    }

    /**
     * @param id_przedmiot the id_przedmiot to set
     */
    public void setId_przedmiot(int id_przedmiot) {
        this.id_przedmiot = id_przedmiot;
    }

    /**
     * @return the id_student
     */
    public int getId_student() {
        return id_student;
    }

    /**
     * @param id_student the id_student to set
     */
    public void setId_student(int id_student) {
        this.id_student = id_student;
    }
}