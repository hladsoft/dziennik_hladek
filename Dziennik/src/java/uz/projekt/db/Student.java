/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uz.projekt.db;

/**
 *
 * @author Rain
 */
public class Student {

    private int id;
    private long pesel;
    private int nrIndeksu;
    private String imie;
    private String nazwisko;
    private int rokStudiow;
    private int semestr; //brak w bazie ? Dodaj do tabeli rok ?
    private int idGrupa;
    private int rok;

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the pesel
     */
    public long getPesel() {
        return pesel;
    }

    /**
     * @param pesel the pesel to set
     */
    public void setPesel(long pesel) {
        this.pesel = pesel;
    }

    /**
     * @return the nrIndeksu
     */
    public int getNrIndeksu() {
        return nrIndeksu;
    }

    /**
     * @param nrIndeksu the nrIndeksu to set
     */
    public void setNrIndeksu(int nrIndeksu) {
        this.nrIndeksu = nrIndeksu;
    }

    /**
     * @return the imie
     */
    public String getImie() {
        return imie;
    }

    /**
     * @param imie the imie to set
     */
    public void setImie(String imie) {
        this.imie = imie;
    }

    /**
     * @return the nazwisko
     */
    public String getNazwisko() {
        return nazwisko;
    }

    /**
     * @param nazwisko the nazwisko to set
     */
    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    /**
     * @return the rokStudiow
     */
    public int getRokStudiow() {
        return rokStudiow;
    }

    /**
     * @param rokStudiow the rokStudiow to set
     */
    public void setRokStudiow(int rokStudiow) {
        this.rokStudiow = rokStudiow;
    }

    /**
     * @return the semestr
     */
    public int getSemestr() {
        return semestr;
    }

    /**
     * @param semestr the semestr to set
     */
    public void setSemestr(int semestr) {
        this.semestr = semestr;
    }

    /**
     * @return the idGrupa
     */
    public int getIdGrupa() {
        return idGrupa;
    }

    /**
     * @param idGrupa the idGrupa to set
     */
    public void setIdGrupa(int idGrupa) {
        this.idGrupa = idGrupa;
    }

    /**
     * @return the rok
     */
    public int getRok() {
        return rok;
    }

    /**
     * @param rok the rok to set
     */
    public void setRok(int rok) {
        this.rok = rok;
    }

}
