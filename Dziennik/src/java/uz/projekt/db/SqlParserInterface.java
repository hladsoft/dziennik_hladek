/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uz.projekt.db;

/**
 *
 * @author Rain
 */
public interface SqlParserInterface {
    
String createAddQuery(Student student);
String createDeleteQuery(Student student) ;
String createUpdateQuery(Student student) ;
String createReadOneQuery(Student student); //where studId=student.getID() ->szukanie po ID -> potrzebne do edycji
String SearchStudent(Student student); // szukanie po peselu || nazwisku || ,jak tam wymyslisz
String createReadAllQuery(Student student);


//SPRAWDZ KLASE OCENA-> zawiera ona w sobie pole przedmiot
String createAddQuery(Ocena ocena, Student student); // where studentId=student.getId() , ocena=ocena.getStopien() , przemiot = ocena.getPrzedmiot;
String createDeleteQuery(Ocena ocena, Student student,Przedmiot przedmiot) ; //w jaki sposob odwolamy sie do konkretnej oceny jesli nie mamy jej ID ? TODO (poprzez wartosc ?)
String createUpdateQuery(Ocena ocena, Student student) ; //w jaki sposob odwolamy sie do konkretnej oceny jesli nie mamy jej ID ? TODO (poprzez wartosc ?)
String createReadOneQuery(Ocena ocena, Student student); 
String createReadAllQuery(Ocena ocena, Student student); //WHERE idStudent=student.getId()  -> dotycza one wszystkich ocen studenta

String createAddQuery(Wykladowca wykladowca);
String createDeleteQuery(Wykladowca wykladowca) ;
String createUpdateQuery(Wykladowca wykladowca) ;
String createReadOneQuery(Wykladowca wykladowca);
String createReadAllQuery(Wykladowca wykladowca);

String createAddQuery(Przedmiot przedmiot);
String createDeleteQuery(Przedmiot przedmiot) ;
String createUpdateQuery(Przedmiot przedmiot) ;
String createReadOneQuery(Przedmiot przedmiot);
String createReadAllQuery(Przedmiot przedmiot);

String createAddQuery(Grupa grupa);
String createDeleteQuery(Grupa grupa) ;
String createUpdateQuery(Grupa grupa) ;
String createReadOneQuery(Grupa grupa);
String createReadAllQuery(Grupa grupa);

String createAddQuery(Rok rok);
String createDeleteQuery(Rok rok) ;
String createUpdateQuery(Rok rok) ;
String createReadOneQuery(Rok rok);
String createReadAllQuery(Rok rok);
//z kontami jeszcze rozwazymy co zrobic

}
