/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uz.projekt.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Rain
 */
public class ConnectionManagerBackup {

    protected static Connection conn = null;
    protected static Statement stmt = null;
    protected final static String DBURL = "jdbc:mysql://localhost:3306/dziennik"; //daj swoje dane!
    protected final static String DBUSER = "root"; //up
    protected final static String DBPASS = "root"; //up
    protected final static String DBDRIVER = "com.mysql.jdbc.Driver"; //pamietaj o dodaniu biblioteki !
    SqlParser sqlParser;

    public ConnectionManagerBackup() {
        sqlParser = new SqlParser();
    }

    public void createConnection() {
        try {
            Class.forName(DBDRIVER).newInstance();
            conn = DriverManager.getConnection(DBURL, DBUSER, DBPASS);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            //throw new ProjectConnectionException(ex.getMessage());           
        }
    }

    //STUDENT
    public List selectStudents() {
        String query = sqlParser.createReadAllQuery();  //checkme
        Student student;
        List studentList = new ArrayList();
        try {
            stmt = conn.createStatement();
            ResultSet results = stmt.executeQuery(query);
            while (results.next()) {
                student = new Student();
                student.setId(results.getInt("id_student"));
                student.setIdGrupa(results.getInt("id_grupa"));
                student.setImie(results.getString("imie"));
                student.setNazwisko(results.getString("nazwisko"));
                student.setPesel(results.getLong("pesel"));
                student.setNrIndeksu(results.getInt("nr_indeksu")); //KURWA MAC
                student.setRok(results.getInt("rok"));
                studentList.add(student);
            }

        } catch (SQLException sqlExcept) {
            sqlExcept.printStackTrace();
        }
        return studentList;
    }

    public void addStudent(Student student) {
        String query = sqlParser.createAddQuery(student);
        try {
            stmt = conn.createStatement();
            stmt.executeUpdate(query);
            stmt.close();
        } catch (SQLException err) {
            System.out.println(err.getMessage());;

        }
    }

    public void deleteObject(Student student) {
        String query = sqlParser.createDeleteQuery(student);
        try {
            stmt = conn.createStatement();
            stmt.executeUpdate(query);
            stmt.close();
        } catch (SQLException err) {
            System.out.println(err.getMessage());;

        }
    }

    public void editObject(Student student) {
        String query = sqlParser.createUpdateQuery(student);
        try {
            stmt = conn.createStatement();
            stmt.executeUpdate(query);
            stmt.close();
        } catch (SQLException err) {
            System.out.println(err.getMessage());;
        }
    }

    public void readOneObject(Student student) {
        String query = sqlParser.createReadOneQuery(student);
        try {
            stmt = conn.createStatement();
            ResultSet results = stmt.executeQuery(query);
            results.next();
            student.setIdGrupa(results.getInt("id_grupa"));
            student.setImie(results.getString("imie"));
            student.setNazwisko(results.getString("nazwisko"));
            student.setPesel(results.getLong("pesel"));
            student.setNrIndeksu(results.getInt("nr_indeksu"));
            student.setRok(results.getInt("rok"));
            results.close();
        } catch (SQLException err) {
            System.out.println(err.getMessage());;
        }
    }

    //WYKLADOWCY
    public List selectWykladowcy() {
        String query = sqlParser.createReadAllWykladowcy();  //checkme
        Wykladowca wykladowca;
        List wykladowcyList = new ArrayList();
        try {
            stmt = conn.createStatement();
            ResultSet results = stmt.executeQuery(query);
            while (results.next()) {
                wykladowca = new Wykladowca();
                wykladowca.setIdWykladowca(results.getInt("id_wykladowca"));
                wykladowca.setNazwisko(results.getString("nazwisko"));
                wykladowca.setImie(results.getString("imie"));
                //wykladowca.setIdLogin(results.getInt(4));
                wykladowcyList.add(wykladowca);
            }
        } catch (SQLException sqlExcept) {
            sqlExcept.printStackTrace();
        }
        return wykladowcyList;
    }

    public void addWykladowca(Wykladowca wykladowca) {
        String query = sqlParser.createAddQuery(wykladowca);
        try {
            stmt = conn.createStatement();
            stmt.executeUpdate(query);
            stmt.close();
        } catch (SQLException err) {
            System.out.println(err.getMessage());

        }
    }

    public void deleteWykladowca(Wykladowca wykladowca) {
        String query = sqlParser.createDeleteQuery(wykladowca);
        try {
            stmt = conn.createStatement();
            stmt.executeUpdate(query);
            stmt.close();
        } catch (SQLException err) {
            System.out.println(err.getMessage());
        }
    }

    public void editWykladowca(Wykladowca wykladowca) {
        String query = sqlParser.createUpdateQuery(wykladowca);
        try {
            stmt = conn.createStatement();
            stmt.executeUpdate(query);
            stmt.close();
        } catch (SQLException err) {
            System.out.println(err.getMessage());;
        }
    }

    public void readOneWykladowca(Wykladowca wykladowca) {
        String query = sqlParser.createReadOneQuery(wykladowca);
        try {
            stmt = conn.createStatement();
            ResultSet results = stmt.executeQuery(query);
            results.next();
            wykladowca.setNazwisko(results.getString("nazwisko"));
            wykladowca.setImie(results.getString("imie"));
            //wykladowca.setIdLogin(results.getInt(4));
            results.close();
        } catch (SQLException err) {
            System.out.println(err.getMessage());;
        }
    }
//KONTA

    public List selectKonto() {
        String query = sqlParser.createReadAllKonta();  //checkme
        Konto konto;
        List kontaList = new ArrayList();
        try {
            stmt = conn.createStatement();
            ResultSet results = stmt.executeQuery(query);
            while (results.next()) {
                konto = new Konto();
                konto.setLogin(results.getString("login"));
                konto.setPassword(results.getString("haslo"));
                konto.setIsAdmin(results.getByte("is_admin"));
                kontaList.add(konto);
            }
        } catch (SQLException sqlExcept) {
            sqlExcept.printStackTrace();
        }
        return kontaList;
    }

    public void addKonto(Konto konto) {
        String query = sqlParser.createAddQuery(konto);
        try {
            stmt = conn.createStatement();
            stmt.executeUpdate(query);
            stmt.close();
        } catch (SQLException err) {
            System.out.println(err.getMessage());

        }
    }

    public void deleteKonto(Konto konto) {
        String query = sqlParser.createDeleteQuery(konto);
        try {
            stmt = conn.createStatement();
            stmt.executeUpdate(query);
            stmt.close();
        } catch (SQLException err) {
            System.out.println(err.getMessage());
        }
    }

    public void editKonto(Konto konto) {
        String query = sqlParser.createUpdateQuery(konto);
        try {
            stmt = conn.createStatement();
            stmt.executeUpdate(query);
            stmt.close();
        } catch (SQLException err) {
            System.out.println(err.getMessage());
        }
    }

    public void readOneKonto(Konto konto) {
        String query = sqlParser.createReadOneQuery(konto);
        try {
            stmt = conn.createStatement();
            ResultSet results = stmt.executeQuery(query);
            results.next();
            konto.setLogin(results.getString("login"));
            konto.setPassword(results.getString("haslo"));
            konto.setIsAdmin(results.getByte("is_admin"));
            results.close();
        } catch (SQLException err) {
            System.out.println(err.getMessage());
        }
    }

    //TEST
    
    
    public <T> void addT(T object) {
        String query = "";
        if (object instanceof Konto) {
            query = sqlParser.createAddQuery((Konto) object);
        }
        if (object instanceof Student) {
            query = sqlParser.createAddQuery((Student) object);
        }
        if (object instanceof Wykladowca) {
            query = sqlParser.createAddQuery((Wykladowca) object);
        }
        try {
            stmt = conn.createStatement();
            stmt.executeUpdate(query);
            stmt.close();
        } catch (SQLException err) {
            System.out.println(err.getMessage());

        }
    }

    public <T> void deleteT(T object) {
        String query = "";
        if (object instanceof Konto) {
            query = sqlParser.createDeleteQuery((Konto) object);
        }
        if (object instanceof Student) {
            query = sqlParser.createDeleteQuery((Student) object);
        }
        if (object instanceof Wykladowca) {
            query = sqlParser.createDeleteQuery((Wykladowca) object);
        }
        try {
            stmt = conn.createStatement();
            stmt.executeUpdate(query);
            stmt.close();
        } catch (SQLException err) {
            System.out.println(err.getMessage());
        }
    }

    public <T> void editT(T object) {
        String query = "";
        if (object instanceof Konto) {
            query = sqlParser.createUpdateQuery((Konto) object);
        }
        if (object instanceof Student) {
            query = sqlParser.createUpdateQuery((Student) object);
        }
        if (object instanceof Wykladowca) {
            query = sqlParser.createUpdateQuery((Wykladowca) object);
        }
        try {
            stmt = conn.createStatement();
            stmt.executeUpdate(query);
            stmt.close();
        } catch (SQLException err) {
            System.out.println(err.getMessage());
        }
    }

    public <T> void readOneT(T object) {
        String query = "";
        if (object instanceof Konto) {
            Konto konto = (Konto) object;
            query = sqlParser.createReadOneQuery(konto);
            try {
                stmt = conn.createStatement();
                ResultSet results = stmt.executeQuery(query);
                results.next();
                konto.setLogin(results.getString("login"));
                konto.setPassword(results.getString("haslo"));
                konto.setIsAdmin(results.getByte("is_admin"));
                results.close();
            } catch (SQLException err) {
                System.out.println(err.getMessage());
            }
        }
        if (object instanceof Student) {
            Student student = (Student) object;
            query = sqlParser.createReadOneQuery(student);
            try {
                stmt = conn.createStatement();
                ResultSet results = stmt.executeQuery(query);
                results.next();
                student.setIdGrupa(results.getInt("id_grupa"));
                student.setImie(results.getString("imie"));
                student.setNazwisko(results.getString("nazwisko"));
                student.setPesel(results.getLong("pesel"));
                student.setNrIndeksu(results.getInt("nr_indeksu"));
                student.setRok(results.getInt("rok"));
                results.close();
            } catch (SQLException err) {
                System.out.println(err.getMessage());;
            }
        }
        if (object instanceof Wykladowca) {
            Wykladowca wykladowca = (Wykladowca) object;
            query = sqlParser.createReadOneQuery(wykladowca);
            try {
                stmt = conn.createStatement();
                ResultSet results = stmt.executeQuery(query);
                results.next();
                wykladowca.setNazwisko(results.getString("nazwisko"));
                wykladowca.setImie(results.getString("imie"));
                //wykladowca.setIdLogin(results.getInt(4));
                results.close();
            } catch (SQLException err) {
                System.out.println(err.getMessage());;
            }
        }

    }

    public void closeConnection() {
        try {
            if (stmt != null) {
                stmt.close();
            }
            if (conn != null) {
                DriverManager.getConnection(DBURL, DBUSER, DBPASS + ";shutdown=true");
                conn.close();
            }
        } catch (SQLException sqlExcept) {
        }

    }

    public String readPass(Konto user) { //TODO poprawic uprawnienia! filip dodaj boolean do konto o nazwie ADMIN: bedzie definiowac czy konto jest adminem czy nie, po dodaniu NAPRAWIC LGOOWANIE tu i w WebPages/logowanie
        String query = "SELECT haslo FROM konto WHERE login='" + user.getLogin() + "';";
        String pass = null;
        try {
            stmt = conn.createStatement();
            ResultSet results = stmt.executeQuery(query);
            results.next();
            pass = results.getString("haslo");
            results.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            System.out.println("error");
            e.printStackTrace();
        }
        return pass;
    }

    public byte readIsAdmin(Konto user) {
        String query = "SELECT is_admin FROM konto WHERE login='" + user.getLogin() + "';";
        byte isAdmin = -1;
        try {
            stmt = conn.createStatement();
            ResultSet results = stmt.executeQuery(query);
            results.next();
            isAdmin = results.getByte("is_admin");
            results.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            System.out.println("error");
            e.printStackTrace();
        }
        return isAdmin;
    }

    public boolean userExists(Konto user) {
        String passwordFromSource = readPass(user);
        if (passwordFromSource != null) {
            return user.getPassword().equals(passwordFromSource);
        } else {
            return false;
        }
    }

    public boolean userIsAdmin(Konto user) {
        if (this.readIsAdmin(user) == 1) {
            return true;
        } else {
            return false;
        }
    }

}
