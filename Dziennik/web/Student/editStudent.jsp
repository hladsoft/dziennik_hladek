<%-- 
    Document   : editStudent
    Created on : 2017-05-08, 15:47:01
    Author     : Rain
--%>

<%@page import="java.util.List"%>
<%@page import="uz.projekt.db.Grupa"%>
<%@page import="uz.projekt.db.Grupa"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="studentx" scope="page" class="uz.projekt.db.Student" />
<jsp:useBean id="connection" scope="session" class="uz.projekt.db.ConnectionManager" />
<jsp:useBean id="auth" scope="session" class="uz.projekt.auth.AuthClass" />
<jsp:setProperty name="auth" property="*" />

<!DOCTYPE html>
<html>
    <head>
        <% if ((auth.authLevel < 2) || (auth.authLevel == null)) response.sendRedirect("/Dziennik/index.jsp"); %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="Stylesheet" type="text/css" href="/Dziennik/style/style.css" />
        <link href="https://fonts.googleapis.com/css?family=Lato&amp;subset=latin-ext" rel="stylesheet">
    </head>
    <body style="margin: 0px;">
        <jsp:include page="../admin_menu.jsp" />
        <%  Integer x = Integer.parseInt(request.getParameter("id"));
            studentx.setId(x);
            connection.createConnection();
            connection.readOneT(studentx);
            connection.closeConnection();
        %>




        <h1>Studenci:</h1>
        <form action="studentEdited.jsp" method="POST">ID
            <input type="text" name="id" value="<%=studentx.getId()%>" readonly="readonly" /> pesel
            <input type="text" name="pesel" value="<%=studentx.getPesel()%>" required pattern="[0-9]{11}"/> rokStud
            <input type="text" name="rok" value="<%=studentx.getRok()%>" required pattern="[0-9]{1}"/> nr Indeksu
            <input type="text" name="nrIndeksu" value="<%=studentx.getNrIndeksu()%>" required pattern="[0-9]{5}"/> imie
            <input type="text" name="imie" value="<%=studentx.getImie()%>" required pattern="[A-Za-z]{3,}"/> nazwisko
            <input type="text" name="nazwisko" value="<%=studentx.getNazwisko()%>" required pattern="[A-Za-z]{3,}"/> id grupy
            <select name="idGrupa">
                <% connection.createConnection();
                    List<Grupa> list = connection.selectGrupa();
                    while (!list.isEmpty()) {
                        Grupa grupa = list.get(0);
                %>
                <option value="<%=grupa.getId()%>"><%=grupa.getNazwa()%></option>
                <% list.remove(0);
                }
                connection.closeConnection();%>

                <input type="submit" value="Edytuj!" />
        </form>




    </body>
</html>
