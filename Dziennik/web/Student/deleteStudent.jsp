<%-- 
    Document   : deleteStudent
    Created on : 2017-05-08, 11:49:42
    Author     : Rain
--%>



<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="studentx" scope="page" class="uz.projekt.db.Student" />
<jsp:useBean id="connection" scope="session" class="uz.projekt.db.ConnectionManager" />
<jsp:useBean id="auth" scope="session" class="uz.projekt.auth.AuthClass" />
<jsp:setProperty name="auth" property="*" />


<!DOCTYPE html>
<html>
    <head>
        <% if ((auth.authLevel < 2) || (auth.authLevel == null)) response.sendRedirect("/Dziennik/index.jsp"); %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="https://fonts.googleapis.com/css?family=Lato&amp;subset=latin-ext" rel="stylesheet">
    </head>
    <body>
        <jsp:include page="../admin_menu.jsp" />
        <h1>Usunieto studenta o ID <%=request.getParameter("id")%></h1>
        <%  Integer x = Integer.parseInt(request.getParameter("id"));
            studentx.setId(x);
            connection.createConnection();
            connection.deleteT(studentx);
            connection.closeConnection();
        %>
        <a href="Student.jsp">HOME</a>

    </body>
</html>
