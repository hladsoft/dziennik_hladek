<%-- 
    Document   : addedS
    Created on : 2017-05-05, 18:34:17
    Author     : Rain
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page errorPage = "../ShowError.jsp" %>
<jsp:useBean id="student" scope="session" class="uz.projekt.db.Student" />
<jsp:setProperty name="student" property="*" />
<jsp:useBean id="connection" scope="session" class="uz.projekt.db.ConnectionManager" />
<jsp:useBean id="auth" scope="session" class="uz.projekt.auth.AuthClass" />
<jsp:setProperty name="auth" property="*" />

<!DOCTYPE html>

<html>
    <head>
        <% if ((auth.authLevel < 2) || (auth.authLevel == null)) response.sendRedirect("/Dziennik/index.jsp"); %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <jsp:include page="../admin_menu.jsp" />
        <% connection.createConnection();
            connection.addT(student);
            connection.closeConnection();%>
        <h1>Student <%=student.getNrIndeksu()%> dodany !</h1>
        <a href="Student.jsp">home</a>
    </body>
</html>
