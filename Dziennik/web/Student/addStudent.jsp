<%-- 
    Document   : addStudent
    Created on : 2017-05-05, 18:32:42
    Author     : Rain
--%>

<%@page import="uz.projekt.db.Grupa"%>
<%@page import="uz.projekt.db.Grupa"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="student" scope="session" class="uz.projekt.db.Student" />
<jsp:useBean id="connection" scope="session" class="uz.projekt.db.ConnectionManager" />
<jsp:useBean id="auth" scope="session" class="uz.projekt.auth.AuthClass" />
<jsp:setProperty name="auth" property="*" />
        
<!DOCTYPE html>
<html>
    <head>
        <% if ((auth.authLevel < 2) || (auth.authLevel == null)) response.sendRedirect("/Dziennik/index.jsp"); %>
        <link rel="Stylesheet" type="text/css" href="/Dziennik/style/style.css" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="https://fonts.googleapis.com/css?family=Lato&amp;subset=latin-ext" rel="stylesheet">
        <title>Dodanie studenta</title>
    </head>
    <body style="margin: 0px;">
        
        <jsp:include page="../admin_menu.jsp" />
        <h1>Dodaj studenta:</h1>
        <form action="addedS.jsp" method="POST">
            <input type="text" name="pesel" placeholder="wpisz pesel" required pattern="[0-9]{11}" />
            <input type="text" name="nrIndeksu" placeholder="nrindeksu" required pattern="[0-9]{5}"/>
            <input type="text" name="imie" placeholder="imie" required pattern="[A-Za-z]{3,}"/>
            <input type="text" name="nazwisko" placeholder="nazwisko" required pattern="[A-Za-z]{3,}"/>
            <input type="text" name="rok" placeholder="rokStudiwo" required pattern="[0-9]{1}"/>
            <select name="idGrupa">
                <% connection.createConnection();
                    List<Grupa> list = connection.selectGrupa();
                    while (!list.isEmpty()) {
                        Grupa grupa = list.get(0);
                %>
                <option value="<%=grupa.getId()%>"><%=grupa.getNazwa()%></option>
                <% list.remove(0);
                }
                connection.closeConnection();%>

                </table>
            </select>
            <input type="submit" value="add" />
        </form>
    </body>
</html>
