<%-- 
    Document   : index
    Created on : 2017-05-05, 18:04:25
    Author     : Rain
--%>

<%@page import="uz.projekt.db.Student"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="connection" scope="session" class="uz.projekt.db.ConnectionManager" />
<jsp:useBean id="auth" scope="session" class="uz.projekt.auth.AuthClass" />
<jsp:setProperty name="auth" property="*" />
<!DOCTYPE html>
<html>
    <head>
        <% if ((auth.authLevel < 2) || (auth.authLevel == null)) response.sendRedirect("/Dziennik/index.jsp"); %>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.bundle.js"></script>
        <link href="https://fonts.googleapis.com/css?family=Lato&amp;subset=latin-ext" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="Stylesheet" type="text/css" href="/Dziennik/style/style.css" />
        <title>eSES - Studenci</title>
    </head>
    <body>
        <jsp:include page="../admin_menu.jsp" />
        <div id="container" style="position: absolute; width: 50%; left: 25%; font-family: 'Lato', sans-serif ">
            <h1 style=" text-align: center; font-family: 'Lato', sans-serif ">Lista studentów</h1>
            <hr/>
            <div>
                <form action="addStudent.jsp">
                    <input class="btn btn-primary" type="submit" value="Dodaj studenta" style="float: left; 
                           margin:10px; " />
                </form>
                <form action="editStudent.jsp" id="form1">
                    <input class="btn btn-primary" type="submit" value="Edytuj studenta" style="float: left; margin:10px; 
                           " />
                    <input class="btn btn-primary" type="submit" value="Usuń studenta" formaction="deleteStudent.jsp" style="float: left; margin:10px; 
                           " />
                </form>
            </div>
            <table class="table" border="1">
                <tr>
                    <td><b>check</b></td>
                    <td><b>ID</b></td>
                    <td><b>Pesel</b></td>
                    <td><b>Numer indeksu</b></td>
                    <td><b>Imię</b></td>
                    <td><b>Nazwisko</b></td>
                    <td><b>Rok Studiów</b></td>
                    <td><b>Grupa</b></td>

                </tr>
                <% connection.createConnection();
                    List<Student> list = connection.selectStudents();
                    while (!list.isEmpty()) {
                        Student student = list.get(0);
                %>
                <tr>

                    <td><input type="radio" name="id" value="<%=student.getId()%>" form="form1" text="id" required/></td> 
                    <td><%=student.getId()%></td>
                    <td><%=student.getPesel()%></td>
                    <td><%=student.getNrIndeksu()%></td>
                    <td><%=student.getImie()%></td>
                    <td><%=student.getNazwisko()%></td>
                    <td><%=student.getRok()%></td>
                    <td><%=connection.getNameGroup(student.getIdGrupa())%></td>
                </tr>

                <% list.remove(0);
                    }
                    connection.closeConnection();%>
            </table>
            <a class="btn btn-primary" style="left: 35%; position: absolute;" href="../admin_main.jsp">Strona Główna</a>
        </div>
    </body>
</html>
