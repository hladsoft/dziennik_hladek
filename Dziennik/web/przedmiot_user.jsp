<%-- 
    Document   : index
    Created on : 2017-05-05, 18:04:25
    Author     : Rain

Ta strona ma wygladac jak w specyfikacji to co nam dali, to bedzie glowna strona do wykonywania operacji wszelakich
--%>

<%@page import="uz.projekt.db.Przedmiot"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="connection" scope="session" class="uz.projekt.db.ConnectionManager" />
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.bundle.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <title>User main page</title>
        <script>
            $(document).ready(function () {
                $rows = $('#tabela tbody tr');
                $('#szukaj').keyup(function () {

                    var val = '^(?=.*\\b' + $.trim($(this).val()).split(/\s+/).join('\\b)(?=.*\\b') + ').*$',
                            reg = RegExp(val, 'i'),
                            text;

                    $rows.show().filter(function () {
                        text = $(this).text().replace(/\s+/g, ' ');
                        return !reg.test(text);
                    }).hide();
                });
            });
        </script>  

    </head>
    <body>
        
        <div id="container" style="position: absolute; width: 50%; left: 25%; font-family: 'Lato', sans-serif ">
            <h1 style=" text-align: center; font-family: 'Lato', sans-serif ">Lista przedmiotów</h1>
            <hr/>
            
            <form action="ocena/showGradesPrzedmioty.jsp" id="form1">
                <input class="btn btn-primary" type="submit" value="Pokaz oceny" />
            </form>
            <input class="form-control" type="text" name="szukaj" placeholder="szukaj" id="szukaj"/>
            <table class="table" id="tabela"  border="1" >    
                <thead>
                    <tr>
                        <td><b>check</b></td>
                        <td><b>ID Przedmiotu</b></td>
                        <td><b>Nazwa</b></td>
                        <td><b>ID Wykładowcy</b></td>
                    </tr>
                </thead>
                <% connection.createConnection();
                    List<Przedmiot> list = connection.selectPrzedmiot();
                    while (!list.isEmpty()) {
                        Przedmiot przedmiot = list.get(0);
                %>
                <tbody>
                    <tr>
                        <td><input type="radio" name="id" value="<%=przedmiot.getId()%>" form="form1" text="id" required/></td> 
                        <td><%=przedmiot.getId()%></td>
                        <td><%=przedmiot.getNazwa()%></td>
                        <td><%=connection.getNameWykladowca(przedmiot.getIdWykladowcy())%></td>
                    </tr>
                    <% list.remove(0);
                        }
                        connection.closeConnection();%>
                </tbody>
            </table>
            <a class="btn btn-primary" style="left: 45%; position: absolute;" href="../user_main.jsp">Strona Główna</a>
    </body>
</html>
