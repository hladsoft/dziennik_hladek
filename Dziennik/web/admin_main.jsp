<%-- 
    Document   : index
    Created on : 2017-05-05, 18:04:25
    Author     : Rain
--%>

<%@page import="uz.projekt.db.Student"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="connection" scope="session" class="uz.projekt.db.ConnectionManager" />

<!DOCTYPE html>
<html>
    <head>

    
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
       
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Menu admina</title>
    </head>
    <body>
        <div id="container" style=" width: 50%;  font-family: 'Lato', sans-serif ">
            <div class="well" style="position: absolute; 
                 top: 20%;
                 left: 25%;
                 margin: 20px;
                 width: 50%;
                 ">
                <form action="Student/Student.jsp">
                    <input class="btn btn-primary" style="width: 100%;margin:10px; 
padding:10px;" type="submit" value="Student" />
                </form>
                <form action="Wykladowca/Wykladowca.jsp">
                    <input class="btn btn-primary" style="width: 100%;margin:10px; 
                           padding:10px;"type="submit" value="Wykladowca" />
                </form>
                <form action="Konto/Konto.jsp">
                    <input  class="btn btn-primary" style="width: 100%;margin:10px; 
                            padding:10px;" type="submit" value="Konto" />
                </form>
                <form action="Grupa/Grupa.jsp">
                    <input  class="btn btn-primary" style="width: 100%;margin:10px; 
                            padding:10px;"  type="submit" value="Grupa" />
                </form>
                <form action="Przedmiot/Przedmiot.jsp">
                    <input  class="btn btn-primary" style="width: 100%;margin:10px; 
                            padding:10px;"  type="submit" value="Przedmiot" />
                </form>
                <a class="btn btn-danger"  style="width: 100%;margin:10px; 
padding:10px;" href="index.jsp">Wyloguj</a>
            </div>
        </div>
    </body>
</html>
