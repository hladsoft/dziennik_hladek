<%-- 
    Document   : addStudent
    Created on : 2017-05-05, 18:32:42
    Author     : Rain
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="grupa" scope="session" class="uz.projekt.db.Grupa" />
<jsp:useBean id="conncection" scope="session" class="uz.projekt.db.ConnectionManager" />
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Dodaj grupe:</h1>
        <form action="addedG.jsp" method="POST">
            <input type="text" name="nazwa" placeholder="wpisz nazwe" required pattern="[-A-Za-z0-9]{3,}" />
            <input type="submit" value="add" />
        </form>
    </body>
</html>
