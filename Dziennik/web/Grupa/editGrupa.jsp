<%-- 
    Document   : editStudent
    Created on : 2017-05-08, 15:47:01
    Author     : Rain
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="grupax" scope="page" class="uz.projekt.db.Grupa" />
<jsp:useBean id="connection" scope="session" class="uz.projekt.db.ConnectionManager" />
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%  Integer x = Integer.parseInt(request.getParameter("id"));
            grupax.setId(x);
            connection.createConnection();
            connection.readOneT(grupax);
            connection.closeConnection();
        %>
        <h1>Hello World!</h1>
        <form action="grupaEdited.jsp" method="POST">
            <input type="text" name="id" value="<%=grupax.getId()%>" readonly="readonly" />
            <input type="text" name="nazwa" value="<%=grupax.getNazwa()%>" required pattern="[-A-Za-z0-9]{3,}" />
            <input type="submit" value="Edytuj!" />
        </form>
    </body>
</html>
