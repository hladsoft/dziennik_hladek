<%-- 
    Document   : addedS
    Created on : 2017-05-05, 18:34:17
    Author     : Rain
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page errorPage = "../ShowError.jsp" %>
<jsp:useBean id="grupa" scope="session" class="uz.projekt.db.Grupa" />
<jsp:setProperty name="grupa" property="*" />
<jsp:useBean id="connection" scope="session" class="uz.projekt.db.ConnectionManager" />
<!DOCTYPE html>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>

        <% connection.createConnection();
            connection.addT(grupa);
            connection.closeConnection();%>
        <h1>Grupa <%=grupa.getNazwa()%> dodana !</h1>
        <a href="Grupa.jsp">home</a>
    </body>
</html>
