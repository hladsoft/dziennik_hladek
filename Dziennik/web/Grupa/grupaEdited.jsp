<%-- 
    Document   : studentEdited
    Created on : 2017-05-08, 15:55:39
    Author     : Rain
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page errorPage = "../ShowError.jsp" %>
<jsp:useBean id="connection" scope="session" class="uz.projekt.db.ConnectionManager" />
<jsp:useBean id="grupax" scope="page" class="uz.projekt.db.Grupa" />
<jsp:setProperty name="grupax" property="*" />
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Grupa  <%=grupax.getId()%> zmieniona!</h1>
        <%  Integer x = Integer.parseInt(request.getParameter("id"));
            grupax.setId(x);
            connection.createConnection();
            connection.editT(grupax);
            connection.closeConnection();
        %>
        <a href="Grupa.jsp">HOME</a>
    </body>
</html>
