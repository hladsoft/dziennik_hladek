<%-- 
    Document   : ShowError
    Created on : 2017-05-27, 15:11:28
    Author     : Rain
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page isErrorPage = "true" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Error</title>
    </head>
    <body>
        <p>Podczas przetwarzania żądania wystapił błąd, powróć do poprzedniej strony i upewnij się, że wprowadzasz poprawne dane</p>
        <a href="javascript:history.back()">POWROT</a>
    </body>
</html>