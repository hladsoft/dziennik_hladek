<%--
    Document   : showGrades
    Created on : 2017-05-14, 23:01:49
    Author     : Rain
--%>

<%@page import="uz.projekt.db.Przedmiot"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Iterator"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="studentx" scope="session" class="uz.projekt.db.Student" />
<jsp:useBean id="stuprzed" scope="session" class="uz.projekt.db.StudentPrzedmiot" />
<jsp:useBean id="gradeConnection" scope="session" class="uz.projekt.db.GradeManager" />
<!--Load the AJAX API-->


<!DOCTYPE html>
<html>
    <head>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.bundle.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.4/jspdf.debug.js"</script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link href="https://fonts.googleapis.com/css?family=Lato&amp;subset=latin-ext" rel="stylesheet">
        <link rel="Stylesheet" type="text/css" href="/Dziennik/style/style.css" />

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>eSES - Oceny Studenta</title>
        <%
            if (request.getParameter("id") != null) {
                Integer x = Integer.parseInt(request.getParameter("id"));
                studentx.setId(x);
                gradeConnection.createConnection();
                gradeConnection.selectGrades(studentx);
                stuprzed.setStudent_id(x);
            }
        %>
    </head>

    <body id="body">

        <jsp:include page="../user_menu.jsp" />
        <div id="container" style="position: absolute; width: 70%; left: 15%; font-family: 'Lato', sans-serif ">
            <h1 style=" text-align: center; font-family: 'Lato', sans-serif ">Oceny studenta</h1>
            <hr/>

                <div class="square" style="float:left">
                    <%--Na ten moment obiekt studenta przechowuje tylko jego id, jak chcemy pobrac z bazy  reszte z jakiego powodu, tzeba uzyc readOne()--%>
                    <h2><%=gradeConnection.matchNamesStudent(stuprzed.getStudent_id())%></h2>
                    <hr/>
                    <form  id="form1">

                        <input class="btn btn-primary" type="submit" formaction="Ocena.jsp" value="Ocena" style="float: left; margin:1px;
                       " />
                        <input class="btn btn-primary" type="submit" formaction="addOcenaStudent.jsp" value="Dodaj ocenę" style="float: left; margin:1px;
                       " />
                        <input class="btn btn-primary" type="submit" formaction="setOcenaK_S.jsp" value="Wystaw oceny końcowe" style="float: left; margin:1px;
                       " />

                        <input class="btn btn-primary" onclick="proponowana_ocena()" type="button" value="Sugerowane Oceny" style="float: left; margin:1px;
                       " />
                        <input class="btn btn-primary" onclick="generation_pdf()" type="button" value="Raport" style="float: left; margin:1px;
                       " />


                    </form>
                    <table class="table" id="tabela" >
                        <tr>

                            <%--  do dołaczenia, na potrzeby funkcji rysuj wykres, zeby wybrac przedmiot<input type="radio" name="id" value="<%=student.getId()%>" form="form1" text="id" required/>  --%>
                            <td>check</td>
                            <td>Przedmiot</td>
                            <td>Oceny</td>
                            <td>Srednia</td>
                            <td>Sugerowana ocena</td>
                            <td>Ocena Koncowa</td>


                        </tr>

                        <%
                            Iterator<Integer> iter = gradeConnection.selectClasses(studentx);
                            gradeConnection.ocenyKoncowe(studentx);
                            HashMap<Integer, Float> finalMap = gradeConnection.getFinalGradeMap();
                            Integer tmp = null;
                            HashMap<Integer, ArrayList<Float>> map = gradeConnection.getGradeMap();
                            while (iter.hasNext()) {
                                tmp = iter.next();
                        %>
                        <tr >
                            <td><input type="radio"
                                       name="id_przedmiot"
                                       value="<%=tmp%>"
                                       form="form1"
                                       text="id" required/></td>
                            <td><%=gradeConnection.matchNames(tmp)%></td>
                            <td id ="<%=tmp%>"><%=map.get(tmp)%></td>
                            <td id="<%=tmp%>x<%=tmp%>"></td>
                            <td></td>
                            <td><%=finalMap.get(tmp)%></td>

                        </tr>
                        <% }
                            gradeConnection.closeConnection();%>

                    </table>
                </div>
                <div class="square" style="float:right" >
                    <table width="300" height="300">
                        <td>
                            <canvas id="myChart" width="300" height="300"></canvas>
                        </td>
                    </table>
                    <table width="300" height="300">
                        <td>
                            <canvas id="myChartTwo" width="300" height="300"></canvas>
                        </td>
                    </table>


                </div>
                <div style="clear: both"></div>
                <div style=" text-align: center; margin: 0 auto">
                    <a class="btn btn-primary" style="left: 45%; position: absolute;" href="../user_main.jsp">Strona Główna</a>
                </div>


                <script src="../script/script.js"></script> <!-- skrtpy -->
                <script src="../script/raport.js"></script> <!-- skrtpy -->


    </body>

</div>
</html>
