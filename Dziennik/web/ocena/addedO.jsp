<%-- 
    Document   : addedS
    Created on : 2017-05-05, 18:34:17
    Author     : Rain
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page errorPage = "../ShowError.jsp" %>
<jsp:useBean id="ocena" scope="session" class="uz.projekt.db.Ocena" />
<jsp:setProperty name="ocena" property="*" />
<jsp:useBean id="connection" scope="session" class="uz.projekt.db.ConnectionManager" />
<jsp:useBean id="stuprzed" scope="session" class="uz.projekt.db.StudentPrzedmiot" />
<!DOCTYPE html>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>

        <% connection.createConnection();
            connection.addT(ocena);
            connection.closeConnection();
            stuprzed.setId_przedmiotu(ocena.getId_przedmiotu());
            stuprzed.setStudent_id(ocena.getStudent_id());%>
        <h1>Ocena dodana !</h1>
        <a href="Ocena.jsp">Powrot do ocen studenta z <%=connection.getNamePrzedmiot(ocena.getId_przedmiotu())%></a>
    </body>
</html>
