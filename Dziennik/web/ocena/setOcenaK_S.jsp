<%--
    Document   : showGrades
    Created on : 2017-05-14, 23:01:49
    Author     : Rain
--%>

<%@page import="uz.projekt.db.Przedmiot"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Iterator"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="studentx" scope="session" class="uz.projekt.db.Student" />
<jsp:useBean id="stuprzed" scope="session" class="uz.projekt.db.StudentPrzedmiot" />
<jsp:useBean id="gradeConnection" scope="session" class="uz.projekt.db.GradeManager" />
<!--Load the AJAX API-->


<!DOCTYPE html>
<html>
    <head>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.bundle.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <%
            if (request.getParameter("id") != null) {
                Integer x = Integer.parseInt(request.getParameter("id"));
                studentx.setId(x);
                stuprzed.setStudent_id(x);
                }
                gradeConnection.createConnection();
                gradeConnection.selectGrades(studentx);
                
            
        %>
    </head>

    <body id="body">
        <div id="container" style="margin: 0 auto; width: 70%">
            <div class="square" style="float:left"> 
                <h1>Oceny studenta <%=gradeConnection.matchNamesStudent(stuprzed.getStudent_id())%></h1> <%--Na ten moment obiekt studenta przechowuje tylko jego id, jak chcemy pobrac z bazy  reszte z jakiego powodu, tzeba uzyc readOne()--%>

                <form  id="formx">
                   
                
                    <input class="btn btn-primary" onclick="proponowana_ocena()" type="button" value="Sugerowane oceny" />
                    
                    
                </form>
                
                <table class="table" id="tabela" >
                    <tr>

                        <%--  do dołaczenia, na potrzeby funkcji rysuj wykres, zeby wybrac przedmiot<input type="radio" name="id" value="<%=student.getId()%>" form="form1" text="id" required/>  --%>
                        <td>check</td>
                        <td>Przedmiot</td>
                        <td>Oceny</td>
                        <td>Srednia</td>
                        <td>Sugerowana ocena</td>
                        <td>Ocena Koncowa</td>


                    </tr>

                    <%
                        Iterator<Integer> iter = gradeConnection.selectClasses(studentx);
                        gradeConnection.ocenyKoncowe(studentx);
                        Integer tmp = null;
                        HashMap<Integer, ArrayList<Float>> map = gradeConnection.getGradeMap();
                        HashMap<Integer, Float> finalMap = gradeConnection.getFinalGradeMap();
                        while (iter.hasNext()) {
                            tmp = iter.next();
                    %>
                    <tr >
                    <form action="ocenaKoncowaStud.jsp" id="formOK">
                        <td><input type="radio" 
                                   name="id_przedmiot" 
                                   value="<%=tmp%>" 
                                   
                                   text="id" required="true"
                                   checked="checked"/></td> 
                        <td><%=gradeConnection.matchNames(tmp)%></td>
                        <td id ="<%=tmp%>"><%=map.get(tmp)%></td>
                        <td id="<%=tmp%>x<%=tmp%>"></td>
                        <td></td>
                    
                        <td><input type="text"  <%--validajca ! --%>
                                   name="ocenaKoncowa" 
                                   placeholder="<%=finalMap.get(tmp)%>"
                                   value="<%=finalMap.get(tmp)%>"/>
                            <input type="submit" value="Wystaw ocene koncowa" />
                        </td> <%--ocenakoncowa --%>
                    </form>

                    </tr>
                    <% } gradeConnection.closeConnection();%>
                </table>
            </div>
            <div class="square" style="float:left" > 
                <table width="300" height="300">
                    <td>
                        <canvas id="myChart" width="300" height="300"></canvas>
                    </td>
                </table>


            </div>
            <div style="clear: both"></div>
            <div style=" text-align: center; margin: 0 auto">
                <a href="showGrades.jsp">Powrot</a>
                <a href="../user_main.jsp">User Main Page</a>
            </div>
        </div>


        <script type="text/javascript">
            function getGrade(text) {
                grade = text.slice(1, text.length - 1);
                var list = [];
                list = grade.split(",");
                var list_float = [];

                for (i = 0; i < list.length; i++) {
                    list_float.push(parseFloat(list[i]));
                }

                return list_float;
            }

            function average(list_grade) {
                var sum = 0;

                for (i = 0; i < list_grade.length; i++) {
                    sum += list_grade[i];
                }
                var average = sum / list_grade.length;
                return average;
            }
            function round(n, k)
            {
                var factor = Math.pow(10, k);
                return Math.round(n * factor) / factor;
            }
            function checkAdult(all_grade) {
                var two = new Array();
                var two_half = new Array();
                var three = new Array();
                var three_half = new Array();
                var four = new Array();
                var four_half = new Array();
                var five = new Array();

                for (x = 0; x < all_grade.length; x++) {
                    switch (all_grade[x]) {
                        case 2:
                            two.push(all_grade[x]);
                            break;

                        case 2.5:
                            two_half.push(all_grade[x]);
                            break;
                        case 3:
                            three.push(all_grade[x]);
                            break;
                        case 3.5:
                            three_half.push(all_grade[x]);
                            break;
                        case 4:
                            four.push(all_grade[x]);
                            break;
                        case 4.5:
                            four_half.push(all_grade[x]);
                            break;
                        case 5:
                            five.push(all_grade[x]);
                            break;
                    }
                }
                var date = new Array();
                date = [two.length,
                    two_half.length,
                    three.length,
                    three_half.length,
                    four.length,
                    four_half.length,
                    five.length];
                return date;
            }

            function proponowana_ocena() {
                for (var x = 1; x < tr.length; x++) { //pętla po wszystkich td

                    var oceny = tr[x].getElementsByTagName('td')[2]; //ustawiam sie na ocenach

                    var grade = getGrade(oceny.textContent); //zemieniam na stinga


                    var mean = average(grade);

                    var el = tr[x].getElementsByTagName('td')[4]; //ustawiam się na drugi element

                    if (mean < 2.81) {
                        el.textContent = 3; //wstawiam srednia
                    }
                    if (mean > 2.81 && mean < 3.20) {
                        el.textContent = 3; //wstawiam srednia
                    }
                    if (mean > 3.21 && mean < 3.80) {
                        el.textContent = 3.5; //wstawiam srednia
                    }
                    if (mean > 3.81 && mean < 4.20) {
                        el.textContent = 4.0; //wstawiam srednia
                    }
                    if (mean > 4.21 && mean < 4.80) {
                        el.textContent = 4.5; //wstawiam srednia
                    }
                    if (mean > 4.81 && mean < 5.00) {
                        el.textContent = 5.0; //wstawiam srednia
                    }



                    //el.textContent = mean; //wstawiam srednia

                }
            }

            var tab = document.getElementsByTagName("table")[0];

            var tr = tab.getElementsByTagName('tr'); //pobieramy wszystkie td z tabeli

            var all_grade = new Array();
            for (var x = 1; x < tr.length; x++) { //pętla po wszystkich td

                var oceny = tr[x].getElementsByTagName('td')[2]; //ustawiam sie na ocenach

                var grade = getGrade(oceny.textContent); //zemieniam na stinga

                for (var y = 0; y < grade.length; y++) {
                    all_grade.push(grade[y]);
                }

                var mean = average(grade);
                mean = round(mean, 2);

                var el = tr[x].getElementsByTagName('td')[3]; //ustawiam się na drugi element

                el.textContent = mean; //wstawiam srednia

                if (mean <= 2) {
                    el.style.backgroundColor = "red";
                }
                if (mean <= 2.4) {
                    el.style.backgroundColor = "red";
                } else if (mean >= 4.5) {
                    el.style.backgroundColor = "green";
                } else if (mean <= 4.5 && mean >= 2.5) {
                    el.style.backgroundColor = "yellow";
                }



            }

            // WYKRES

            all_grade.sort();




            var date = checkAdult(all_grade);

            var ctx = document.getElementById("myChart");


            var myChart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: ["2.0", "2.5", "3.0", "3.5", "4.0", "4.5", "5.0"],
                    datasets: [{
                            label: 'ilość ocen',
                            data: [date[0], date[1], date[2], date[3], date[4], date[5], date[6]],
                            backgroundColor: [
                                'rgba(255, 99, 132, 0.2)',
                                'rgba(54, 162, 235, 0.2)',
                                'rgba(255, 206, 86, 0.2)',
                                'rgba(75, 192, 192, 0.2)',

                                'rgba(255, 99, 132, 0.2)',
                                'rgba(54, 162, 235, 0.2)',
                                'rgba(255, 206, 86, 0.2)',
                                'rgba(75, 192, 192, 0.2)',
                                'rgba(255, 99, 132, 0.2)'

                            ],
                            borderColor: [
                                'rgba(255,99,132,1)',
                                'rgba(54, 162, 235, 1)',
                                'rgba(255, 206, 86, 1)',
                                'rgba(75, 192, 192, 1)',
                                'rgba(153, 102, 255, 1)',
                                'rgba(255, 159, 64, 1)',
                                'rgba(255, 159, 64, 1)',
                                'rgba(255, 159, 64, 1)'
                            ],
                            borderWidth: 1
                        }]
                },
                options: {

                    scales: {
                        yAxes: [{
                                ticks: {
                                    beginAtZero: true
                                }
                            }]
                    }
                }
            });
        </script>
    </body>


</html>
