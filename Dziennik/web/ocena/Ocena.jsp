<%@page import="uz.projekt.db.Ocena"%>
<%@page import="uz.projekt.db.Wykladowca"%>
<%@page import="uz.projekt.db.Grupa"%>
<%@page import="uz.projekt.db.Przedmiot"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="stuprzed" scope="session" class="uz.projekt.db.StudentPrzedmiot" />
<jsp:useBean id="gradeConnection" scope="session" class="uz.projekt.db.GradeManager" />
<!DOCTYPE html>
<html>
    <head>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.bundle.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <!--WPROWADZANIE ZMIAN W KODZIE WYWALA ZMIENNĄ PRZEZ CO ZNIKAJĄ WARTOŚCI W TABELI TRZEBA COFNĄĆ I WRÓCIĆ--> 
        <%
            if (request.getParameter("id_przedmiot") != null) {
                Integer x = Integer.parseInt(request.getParameter("id_przedmiot"));
                stuprzed.setId_przedmiotu(x);
            }
        %>
        <h1>Oceny z <%=gradeConnection.getNamePrzedmiot(stuprzed.getId_przedmiotu())%> studenta <%=gradeConnection.matchNamesStudent(stuprzed.getStudent_id())%></h1>
        <form action="addOcena.jsp">
            <input class="btn btn-primary" type="submit" value="addOcena" />
        </form>
        <form action="editOcena.jsp" id="form1">
            <input class="btn btn-primary" type="submit" value="editOcena" />
            <input class="btn btn-primary" type="submit" value="deleteOcena" formaction="deleteOcena.jsp"/>
        </form>
        <table class="table" border="1">
            <tr>
                <td><b>check</b></td>
                <td><b>idocena</b></td>
                <td><b>ocena</b></td>
            </tr>
            <% gradeConnection.createConnection();
                List<Ocena> list = gradeConnection.selectOcena(stuprzed);
                while (!list.isEmpty()) {
                    Ocena ocena = list.get(0);
            %>
            <tr>
                <td><input type="radio" name="id" value="<%=ocena.getId_ocena()%>" form="form1" text="id" required/></td> 
                <td><%=ocena.getId_ocena()%></td>
                <td><%=ocena.getOcena()%></td>
            </tr>
            <% list.remove(0);
                }
                gradeConnection.closeConnection();%>
        </table>
        <a href="../user_main.jsp">User Main Page</a>
        <a href="showGrades.jsp?id=<%=stuprzed.getStudent_id()%>">Powrot do ocen studenta</a>
        <a href="showGradesPrzedmioty.jsp?id=<%=stuprzed.getId_przedmiotu()%>">Powrot do ocen z przedmiotu</a>
    </body>
</html>
