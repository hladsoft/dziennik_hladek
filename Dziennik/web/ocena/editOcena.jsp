<%-- 
    Document   : editStudent
    Created on : 2017-05-08, 15:47:01
    Author     : Rain
--%>
<%@page import="java.util.List"%>
<%@page import="uz.projekt.db.Wykladowca"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="ocenax" scope="page" class="uz.projekt.db.Ocena" />
<jsp:useBean id="connection" scope="session" class="uz.projekt.db.ConnectionManager" />
<!DOCTYPE html>
<html>
    <head>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.bundle.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%  Integer x=Integer.parseInt(request.getParameter("id"));
             ocenax.setId_ocena(x); 
         connection.createConnection();
         connection.readOneT(ocenax);
         connection.closeConnection();
        %>
        <h1>Hello World!</h1>
        <form action="ocenaEdited.jsp" method="POST">
            <input class="form-control"type="text" name="id" value="<%=ocenax.getId_ocena()%>" readonly="readonly" />
            <input class="form-control" type="number" name="ocena" value="<%=ocenax.getOcena()%>" min="2" max="5" step="0.5" />
            <input class="btn btn-primary" type="submit" value="Edytuj!" />
        </form>
    </body>
</html>
