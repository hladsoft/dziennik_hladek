<%-- 
    Document   : ocenaKoncowaPrzedm
    Created on : 2017-05-28, 03:29:42
    Author     : Rain
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page errorPage = "../ShowError.jsp" %>
<jsp:useBean id="ocenaK" scope="request" class="uz.projekt.db.OcenaKoncowa" />
<jsp:setProperty name="ocenaK" property="*" />
<jsp:useBean id="gradeConnection" scope="session" class="uz.projekt.db.GradeManager" />
<jsp:useBean id="stuprzed" scope="session" class="uz.projekt.db.StudentPrzedmiot" />

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%
            ocenaK.setId_przedmiot(stuprzed.getId_przedmiotu());
            gradeConnection.createConnection();
            gradeConnection.pushOcenaK(ocenaK);
            gradeConnection.closeConnection();
        %>
        <h1>Ocena wystawiona !</h1>
        <a href="showGradesPrzedmioty.jsp">Powrot</a>
    </body>
</html>