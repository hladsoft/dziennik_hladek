<%@page import="uz.projekt.db.Przedmiot"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="ocena" scope="session" class="uz.projekt.db.Ocena" />
<jsp:useBean id="connection" scope="session" class="uz.projekt.db.ConnectionManager" />
<jsp:useBean id="stuprzed" scope="session" class="uz.projekt.db.StudentPrzedmiot" />
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%
            ocena.setStudent_id(stuprzed.getStudent_id());
        %>
        <h1>Dodaj ocene:</h1>
        <form action="addedO.jsp" method="POST">
            <input class="form-control" type="number" name="ocena" placeholder="wpisz ocene" min="2" max="5" step="0.5" />
            <select name="id_przedmiotu">
                <% connection.createConnection();
                    List<Przedmiot> list = connection.selectPrzedmiot();
                    while (!list.isEmpty()) {
                        Przedmiot przedmiot = list.get(0);
                %>
                <option value="<%=przedmiot.getId()%>"><%=przedmiot.getNazwa()%></option>
                <% list.remove(0);
                }
                connection.closeConnection();%>
                </table>
            </select>
            <input type="submit" value="add" />
        </form>
    </body>
</html>
