<%@page import="uz.projekt.db.Przedmiot"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Iterator"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="przedmiotx" scope="session" class="uz.projekt.db.Przedmiot" />
<jsp:useBean id="stuprzed" scope="session" class="uz.projekt.db.StudentPrzedmiot" />
<jsp:useBean id="gradeConnection" scope="session" class="uz.projekt.db.GradeManager" />
<!DOCTYPE html>
<html>
    <head>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.bundle.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="https://fonts.googleapis.com/css?family=Lato&amp;subset=latin-ext" rel="stylesheet">
        <link rel="Stylesheet" type="text/css" href="/Dziennik/style/style.css" />
        <title>eSES - Oceny</title>
        <%
            if (request.getParameter("id") != null) {
                Integer x = Integer.parseInt(request.getParameter("id"));
                przedmiotx.setId(x);
                gradeConnection.createConnection();
                gradeConnection.selectGrades(przedmiotx);
                stuprzed.setId_przedmiotu(x);
            }
        %>
    </head>

    <body id="body">
        <jsp:include page="../user_menu.jsp" />
        <div id="container" style="position: absolute; width: 70%; left: 15%; font-family: 'Lato', sans-serif ">
            <h1 style=" text-align: center; font-family: 'Lato', sans-serif ">Oceny z przedmiotu <%=gradeConnection.matchNames(przedmiotx.getId())%></h1>
            <hr/> <%--Na ten moment obiekt studenta przechowuje tylko jego id, jak chcemy pobrac z bazy  reszte z jakiego powodu, tzeba uzyc readOne()--%>

            <div class="square" style="float:left">
                <form  id="form1">



                    <input class="btn btn-primary" type="submit" formaction="Ocena.jsp" value="Ocena" style="float: left; margin:1px;
                           " />
                    <input class="btn btn-primary" type="submit" formaction="addOcenaPrzedmiot.jsp" value="Dodaj ocenę" style="float: left; margin:1px;
                           " />
                    <input class="btn btn-primary" type="submit" formaction="setOcenaK_P.jsp" value="Wystaw oceny koncowe" style="float: left; margin:1px;
                           " />
                    <input class="btn btn-primary" onclick="proponowana_ocena()" type="button" value="Sugerowane Oceny" style="float: left; margin:1px;
                           " />

                </form>


                <table  class="table" border="1">
                    <tr>
                        <td>check</td>
                        <td>Student</td>
                        <td>Oceny</td>
                        <td>Średnia</td>
                        <td>Proponowana ocena</td>
                        <td>Ocena końcowa</td>

                    </tr>
                    <%
                        Iterator<Integer> iter = gradeConnection.selectClasses(przedmiotx);
                        gradeConnection.ocenyKoncowe(przedmiotx);
                        HashMap<Integer, Float> finalMap = gradeConnection.getFinalGradeMap();
                        Integer tmp = null;
                        HashMap<Integer, ArrayList<Float>> map = gradeConnection.getGradeMap();
                        while (iter.hasNext()) {
                            tmp = iter.next();
                    %>
                    <tr >
                        <td><input type="radio" name="1" value="" form="form1" text="id" required/></td>
                        <td><%=gradeConnection.matchNamesStudent(tmp)%></td>
                        <td id ="<%=tmp%>"><%=map.get(tmp)%></td>
                        <td id="<%=tmp%>x<%=tmp%>"></td>
                        <td></td>
                        <td><%=finalMap.get(tmp)%></td>

                    </tr>
                    <% }
                        gradeConnection.closeConnection();%>
                </table>
            </div>


            <div class="square" style="float:right">
                <table width="300" height="300">
                    <td>
                        <canvas id="myChart" width="300" height="300"></canvas>
                        <canvas id="myChartTwo" width="300" height="300"></canvas>
                    </td>
                </table>

           <table width="300" height="300">
                <td>
                    <canvas id="myChartTwo" width="300" height="300"></canvas>
                </td>
            </table>
      <script src="../script/script.js"></script> <!-- skrtpy -->


            </div>

            <div style="clear: both"></div>
                <div style=" text-align: center; margin: 0 auto">
                    <a class="btn btn-primary" style="left: 45%; position: absolute;" href="../user_main.jsp">Strona Główna</a>
                </div>


    </body>
</html>
