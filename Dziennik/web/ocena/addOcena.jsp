<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="ocena" scope="session" class="uz.projekt.db.Ocena" />
<jsp:useBean id="connection" scope="session" class="uz.projekt.db.ConnectionManager" />
<jsp:useBean id="stuprzed" scope="session" class="uz.projekt.db.StudentPrzedmiot" />
<!DOCTYPE html>
<html>
    <head>
         <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.bundle.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%
            ocena.setId_przedmiotu(stuprzed.getId_przedmiotu());
            ocena.setStudent_id(stuprzed.getStudent_id());
        %>
        <h1>Dodaj ocene:</h1>
        <form action="addedO.jsp" method="POST">
            <input class="form-control" type="number" name="ocena" placeholder="wpisz ocene" min="2" max="5" step="0.5" />
        </table>
    </select>
    <input class="btn btn-primary" type="submit" value="add" />
</form>
</body>
</html>
