<%@page import="uz.projekt.db.Student"%>
<%@page import="uz.projekt.db.Przedmiot"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="ocena" scope="session" class="uz.projekt.db.Ocena" />
<jsp:useBean id="connection" scope="session" class="uz.projekt.db.ConnectionManager" />
<jsp:useBean id="stuprzed" scope="session" class="uz.projekt.db.StudentPrzedmiot" />
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%
            ocena.setId_przedmiotu(stuprzed.getId_przedmiotu());
        %>
        <h1>Dodaj ocene:</h1>
        <form action="addedO.jsp" method="POST">
            <input class="form-control" type="number" name="ocena" placeholder="wpisz ocene" min="2" max="5" step="0.5" />
            <select name="student_id">
                <% connection.createConnection();
                    List<Student> list = connection.selectStudents();
                    while (!list.isEmpty()) {
                        Student student = list.get(0);
                %>
                <option value="<%=student.getId()%>"><%=student.getImie()%> <%=student.getNazwisko()%></option>
                <% list.remove(0);
                }
                connection.closeConnection();%>
                </table>
            </select>
            <input type="submit" value="add" />
        </form>
    </body>
</html>
