<%-- 
    Document   : loggedAdmin
    Created on : 2017-05-12, 00:51:26
    Author     : Rain
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="connection" scope="session" class="uz.projekt.db.ConnectionManager" />
<jsp:useBean id="user" scope="session" class="uz.projekt.db.Konto" />
<jsp:setProperty name="user" property="*" />
<jsp:useBean id="auth" scope="session" class="uz.projekt.auth.AuthClass" />


<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
        <link href="https://fonts.googleapis.com/css?family=Lato&amp;subset=latin-ext" rel="stylesheet">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>eSES - Zalogowano!</title>
    </head>
    <body>
        <div id="container">
            <div class="well" style="position: absolute; 
                 top: 20%;
                 left: 35%;
                 margin: auto;
                 width: 30%;
                 ">

        <% connection.createConnection(); %>
        <% if (connection.userExists(user) && connection.userIsAdmin(user)) { %>
        <div style="text-align: center;margin: 40px;">
                <h1 style="color: green;center">Zalogowano pomyślnie!</h1>     
                <h1> </h1>
                <a href="../admin_main.jsp" class="btn btn-primary" >  Przejdź do panelu </a>
        </div>
          
        <% connection.closeConnection();
            } 
            auth.setLoggedAsAdmin();
        %>
        <% if (!connection.userExists(user)) { %>
        
        <div style="text-align: center;margin: 40px;">
                <h1 style="color: red;center">Niepoprawne dane!</h1>
                <h1> </h1>
                <a class="btn btn-primary" style="text-align: center;"  href="../index.jsp"> Wróć </a>
        </div>
        
        <% connection.closeConnection();
            } %>
        <% if (connection.userExists(user) && !connection.userIsAdmin(user)) { %>
        <div style="text-align: center;margin: 40px;">
            <h1 style="color: red;center">Brak uprawnień!</h1>
            <h1> </h1>
            <a class="btn btn-primary" style="text-align: center;"  href="../index.jsp"> Wróć </a>
        </div>
        
        <% connection.closeConnection();
            }%>
        </div>
        </div>
    </body>
</html>
