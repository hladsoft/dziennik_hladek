<%-- 
    Document   : user_menu
    Created on : 2017-06-07, 01:58:10
    Author     : mmarc
--%>

<!-- menu -->
<jsp:useBean id="auth" scope="session" class="uz.projekt.auth.AuthClass" />
<jsp:setProperty name="auth" property="*" />

<div id="menuHolder">
    <a href="/Dziennik/student_user.jsp">
        <div class="menuItem">
            Oceny studentów
        </div>
    </a>
        <a href="/Dziennik/przedmiot_user.jsp">
            <div class="menuItem">
                Lista przedmiotów 
            </div>
        </a>
        
        <a href="/Dziennik/logout.jsp">
            <div class="menuItem" id="menuItemLast">
                Wyloguj
            </div>
        </a>
</div>

