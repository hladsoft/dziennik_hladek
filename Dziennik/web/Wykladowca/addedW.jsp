<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page errorPage = "../ShowError.jsp" %>
<jsp:useBean id="wykladowca" scope="session" class="uz.projekt.db.Wykladowca" />
<jsp:setProperty name="wykladowca" property="*" />
<jsp:useBean id="connection" scope="session" class="uz.projekt.db.ConnectionManager" />
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <% connection.createConnection();
            connection.addT(wykladowca);
            connection.closeConnection();%>
        <h1>Wykladowca <%=wykladowca.getNazwisko()%> dodany !</h1>
        <a href="Wykladowca.jsp">home</a>
    </body>
</html>
