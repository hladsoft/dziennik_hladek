<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="wykladowca" scope="session" class="uz.projekt.db.Wykladowca" />
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Dodaj wykladowce:</h1>
        <form action="addedW.jsp" method="POST">
            <input type="text" name="nazwisko" placeholder="nazwisko" required pattern="[A-Za-z]{3,}"/>
            <input type="text" name="imie" placeholder="imie" required pattern="[A-Za-z]{3,}"/>
            <%-- <input type="text" name="idLogin" placeholder="idLogin" required/> --%>
            <input type="submit" value="add" />
        </form>
    </body>
</html>
