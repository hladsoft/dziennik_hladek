<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page errorPage = "../ShowError.jsp" %>
<jsp:useBean id="connection" scope="session" class="uz.projekt.db.ConnectionManager" />
<jsp:useBean id="wykladowcax" scope="page" class="uz.projekt.db.Wykladowca" />
<jsp:setProperty name="wykladowcax" property="*" />
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>wykladowca  <%=wykladowcax.getIdWykladowca()%> edited!</h1>
        <%  Integer x = Integer.parseInt(request.getParameter("idWykladowca"));
            wykladowcax.setIdWykladowca(x);
            connection.createConnection();
            connection.editT(wykladowcax);
            connection.closeConnection();
        %>
        <a href="Wykladowca.jsp">HOME</a>
    </body>
</html>
