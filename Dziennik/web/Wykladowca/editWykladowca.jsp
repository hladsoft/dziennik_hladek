<%-- 
    Document   : editStudent
    Created on : 2017-05-08, 15:47:01
    Author     : Rain
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="wykladowcax" scope="page" class="uz.projekt.db.Wykladowca" />
<jsp:useBean id="connection" scope="session" class="uz.projekt.db.ConnectionManager" />
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%  Integer x = Integer.parseInt(request.getParameter("idWykladowca"));
            wykladowcax.setIdWykladowca(x);
            connection.createConnection();
            connection.readOneT(wykladowcax);
            connection.closeConnection();
        %>
        <h1>Hello World!</h1>
        <form action="wykladowcaEdited.jsp" method="POST">
            <input type="text" name="idWykladowca" value="<%=wykladowcax.getIdWykladowca()%>" readonly="readonly" />
            <input type="text" name="nazwisko" value="<%=wykladowcax.getNazwisko()%>" required pattern="[A-Za-z]{3,}"/>
            <input type="text" name="imie" value="<%=wykladowcax.getImie()%>" required pattern="[A-Za-z]{3,}"/>
            <%-- <input type="text" name="idLogin" value="<%=wykladowcax.getIdLogin()%>" /> --%>
            <input type="submit" value="Edytuj!" />
        </form>




    </body>
</html>
