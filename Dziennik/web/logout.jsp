<%-- 
    Document   : logout
    Created on : 2017-05-30, 14:48:05
    Author     : Domon
--%>
<jsp:useBean id="auth" scope="session" class="uz.projekt.auth.AuthClass" />
<jsp:setProperty name="auth" property="*" />
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="refresh" content="2; URL=/Dziennik/index.jsp">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Wylogowywanie z systemu</title>
    </head>
    <body>
        <% auth.logout(); %>
        <h1>Wylogowano poprawnie</h1>
        <p> W ciągu 2 sekund nastąpi <a href="/Dziennik/index.jsp"> przekierowanie </a></p>
    </body>
</html>
