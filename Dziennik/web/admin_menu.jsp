
<!-- menu -->
<jsp:useBean id="auth" scope="session" class="uz.projekt.auth.AuthClass" />
<jsp:setProperty name="auth" property="*" />

<div id="menuHolder">
    <a href="/Dziennik/Student/Student.jsp">
        <div class="menuItem">
            Studenci
        </div>
    </a>
        <a href="/Dziennik/Wykladowca/Wykladowca.jsp">
            <div class="menuItem">
                Wykladowcy
            </div>
        </a>
        <a href="/Dziennik/Konto/Konto.jsp">
            <div class="menuItem">
                Konta
            </div>
        </a>
        <a href="/Dziennik/Grupa/Grupa.jsp">
            <div class="menuItem">
                Grupy
            </div>
        </a>
        <a href="/Dziennik/Przedmiot/Przedmiot.jsp">
            <div class="menuItem">
                Przedmioty
            </div>
        </a>
        <a href="/Dziennik/logout.jsp">
            <div class="menuItem" id="menuItemLast">
                Wyloguj
            </div>
        </a>
</div>

