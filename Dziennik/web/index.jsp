<%-- 
    Document   : index
    Created on : 2017-05-12, 00:44:05
    Author     : Rain
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
    <link href="https://fonts.googleapis.com/css?family=Lato&amp;subset=latin-ext" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="Stylesheet" type="text/css" href="style/style.css" />
    <title>Dziennik elektroniczny HŁAD-SOFT 2017(R)</title>
    <link rel="Shortcut icon" href="src/ico.ico" />
</head>
<body>
    <div id="container" class="main_container">
        <div class="well" id="loginWindow"> 
            <div style="text-align: center;margin: 40px;">
                <img src="src/logo.png">
                <h1>wybierz typ konta</h1>
            </div>
            <div id="containerForLoginButtons">
                <div id="adminLoginButton" >
                    <button type="button" class="btn btnLocal" data-toggle="modal" data-target="#adminModal">Administrator</button>
                </div>  
                <div id="userLoginButton">
                    <button type="button" class="btn btnLocal" data-toggle="modal" data-target="#userModal">Wykładowca</button>
                </div> 
            </div>
            <footer id="loginFooter">Created by HŁAD-SOFT company</footer>
        </div>
            
<!-- Modal Admin-->
    <div class="modal fade" id="adminModal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body" style="text-align: center;">
                    <h2>Podaj login i hasło</h2>
                    <form action="/Dziennik/logowanie/loggedAdmin.jsp" method="POST">
                        <div class="input" style="width: 500px"> 
                            <input style="width: 70%; margin:  10px; margin-left: 70px" type="text" name="login" required placeholder="Login"/>
                        </div>
                        <div class="input"  style="width: 500px;">
                            <input  style="width: 70%; margin:  10px; margin-left: 70px" type="password" name="password" required placeholder="Hasło"/>
                        </div>
                        <div>
                            <input style="width: 220px;" class="btn btn-primary" type="submit" value="login" />
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Anuluj</button>
                </div>
            </div>
        </div>
  </div>

<!-- Modal User-->
    <div class="modal fade" id="userModal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body" style="text-align: center;">
                    <h2>Podaj login i hasło</h2>
                    <form action="logowanie/loggedUser.jsp" method="POST">
                        <div class="input" style="width: 500px"> 
                            <input style="width: 70%; margin:  10px; margin-left: 70px" type="text" name="login"  required placeholder="Login"/>
                        </div>
                        <div class="input"  style="width: 500px;">
                            <input  style="width: 70%; margin:  10px; margin-left: 70px" type="password"name="password" required placeholder="Hasło"/>
                        </div>
                        <div>
                            <input style="width: 220px; " class="btn btn-primary" type="submit" value="login" />
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Anuluj</button>
                </div>
            </div>
        </div>
  </div>
            
</body>
</html>
