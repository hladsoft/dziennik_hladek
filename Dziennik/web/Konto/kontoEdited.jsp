<%-- 
    Document   : studentEdited
    Created on : 2017-05-08, 15:55:39
    Author     : Rain
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page errorPage = "../ShowError.jsp" %>
<jsp:useBean id="connection" scope="session" class="uz.projekt.db.ConnectionManager" />
<jsp:useBean id="kontox" scope="page" class="uz.projekt.db.Konto" />
<jsp:setProperty name="kontox" property="*" />
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Zmieniono konto o loginie <%=request.getParameter("login")%></h1>
        <%  String x = request.getParameter("login");
            kontox.setLogin(x);
            connection.createConnection();
            connection.editT(kontox);
            connection.closeConnection();
        %>
        <a href="Konto.jsp">HOME</a>
    </body>
</html>
