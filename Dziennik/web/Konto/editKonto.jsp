<%-- 
    Document   : editStudent
    Created on : 2017-05-08, 15:47:01
    Author     : Rain
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="kontox" scope="page" class="uz.projekt.db.Konto" />
<jsp:useBean id="connection" scope="session" class="uz.projekt.db.ConnectionManager" />
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%  String x = request.getParameter("login");
            kontox.setLogin(x);
            connection.createConnection();
            connection.readOneT(kontox);
            connection.closeConnection();
        %>

        <h1>Konto:</h1>
        <form action="kontoEdited.jsp" method="POST">
            login<input type="text" name="login" value="<%=kontox.getLogin()%>" readonly="readonly" />
            <!--            "Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters"-->
            haslo<input type="password" name="password" value="<%=kontox.getPassword()%>" required pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"/>
            <select name="isAdmin">
                <option value="0">nie</option>
                <option value="1">tak</option>
            <input type="submit" value="Edytuj!" />
        </form>




    </body>
</html>
