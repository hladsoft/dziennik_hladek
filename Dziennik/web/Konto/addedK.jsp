<%-- 
    Document   : addedS
    Created on : 2017-05-05, 18:34:17
    Author     : Rain
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page errorPage = "../ShowError.jsp" %>
<jsp:useBean id="konto" scope="session" class="uz.projekt.db.Konto" />
<jsp:setProperty name="konto" property="*" />
<jsp:useBean id="connection" scope="session" class="uz.projekt.db.ConnectionManager" />
<!DOCTYPE html>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>

        <% connection.createConnection();
            connection.addT(konto);
            connection.closeConnection();%>
        <h1>Konto <%=konto.getLogin()%> dodany !</h1>
        <a href="Konto.jsp">home</a>
    </body>
</html>
