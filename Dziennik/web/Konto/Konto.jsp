<%@page import="uz.projekt.db.Konto"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="connection" scope="session" class="uz.projekt.db.ConnectionManager" />
<!DOCTYPE html>
<html>
    <head>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.bundle.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="Stylesheet" type="text/css" href="/Dziennik/style/style.css" />
        <link href="https://fonts.googleapis.com/css?family=Lato&amp;subset=latin-ext" rel="stylesheet">
        <title>eSES - Konta</title>
    </head>
    <body>
        <jsp:include page="../admin_menu.jsp" />
        <div id="container" style="position: absolute; width: 50%; left: 25%; font-family: 'Lato', sans-serif ">
            <h1 style=" text-align: center; font-family: 'Lato', sans-serif ">Lista kont</h1>
            <hr/>
            <form action="addKonto.jsp">
                <input class="btn btn-primary" type="submit" value="Dodaj Konto"  style="float: left; margin:10px; 
                           "/>
            </form>
            <form action="editKonto.jsp" id="form1">
                <input class="btn btn-primary" type="submit" value="Edytuj konto"  style="float: left; margin:10px; 
                           "/>
                <input class="btn btn-primary" type="submit" value="Usuń konto" formaction="deleteKonto.jsp"  style="float: left; margin:10px; 
                           "/>
            </form>
            <table class="table" border="1">
                <tr>
                    <td><b>check</b></td>
                    <td><b>Login</b></td>
                    <td><b>Hasło</b></td>
                    <td><b>Admin?</b></td>
                </tr>
                <% connection.createConnection();
                    List<Konto> list = connection.selectKonto();
                    while (!list.isEmpty()) {
                        Konto konto = list.get(0);
                %>
                <tr>
                    <td><input type="radio" name="login" value="<%=konto.getLogin()%>" form="form1" text="login" required/></td> 
                    <td><%=konto.getLogin()%></td>
                    <td><%=konto.getPassword()%></td>
                    <td><%=konto.getIsAdmin()%></td>
                </tr>
                <% list.remove(0);
                }
                connection.closeConnection();%>
            </table>
            <a class="btn btn-primary" style="left: 35%; position: absolute;" href="../admin_main.jsp">Strona Główna</a>
        </div>
    </body>
</html>
