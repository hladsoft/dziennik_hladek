<%-- 
    Document   : addStudent
    Created on : 2017-05-05, 18:32:42
    Author     : Rain
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="konto" scope="session" class="uz.projekt.db.Konto" />
<jsp:useBean id="conncection" scope="session" class="uz.projekt.db.ConnectionManager" />
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Dodaj konto:</h1>
        <form action="addedK.jsp" method="POST">
            <input type="text" name="login" placeholder="wpisz login" required pattern="[A-Za-z]{3,}" />
<!--            "Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters"-->
            <input type="password" name="password" placeholder="haslo" required pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"/>
            <select name="isAdmin">
                <option value="0">nie</option>
                <option value="1">tak</option>
            <input type="submit" value="add" />
        </form>
    </body>
</html>
