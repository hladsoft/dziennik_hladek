
function getGrade(text) {
    grade = text.slice(1, text.length - 1);
    var list = [];
    list = grade.split(",");
    var list_float = [];

    for (i = 0; i < list.length; i++) {
        list_float.push(parseFloat(list[i]));
    }

    return list_float;
}

function average(list_grade) {
    var sum = 0;

    for (i = 0; i < list_grade.length; i++) {
        sum += list_grade[i];
    }
    var average = sum / list_grade.length;
    return average;
}
function round(n, k)
{
    var factor = Math.pow(10, k);
    return Math.round(n * factor) / factor;
}
function checkAdult(all_grade) {
    var two = new Array();
    var two_half = new Array();
    var three = new Array();
    var three_half = new Array();
    var four = new Array();
    var four_half = new Array();
    var five = new Array();

    for (x = 0; x < all_grade.length; x++) {
        switch (all_grade[x]) {
            case 2:
                two.push(all_grade[x]);
                break;

            case 2.5:
                two_half.push(all_grade[x]);
                break;
            case 3:
                three.push(all_grade[x]);
                break;
            case 3.5:
                three_half.push(all_grade[x]);
                break;
            case 4:
                four.push(all_grade[x]);
                break;
            case 4.5:
                four_half.push(all_grade[x]);
                break;
            case 5:
                five.push(all_grade[x]);
                break;
        }
    }
    var date = new Array();
    date = [two.length,
        two_half.length,
        three.length,
        three_half.length,
        four.length,
        four_half.length,
        five.length];
    return date;
}

function proponowana_ocena() {
    for (var x = 1; x < tr.length; x++) { //pętla po wszystkich td

        var oceny = tr[x].getElementsByTagName('td')[2]; //ustawiam sie na ocenach
        console.log(oceny);
        var grade = getGrade(oceny.textContent); //zemieniam na stinga


        var mean = average(grade);

        var el = tr[x].getElementsByTagName('td')[4]; //ustawiam się na drugi element

        if (mean < 2.81) {
            el.textContent = 2; //wstawiam srednia
        }
        if (mean > 2.81 && mean <= 3.20) {
            el.textContent = 3; //wstawiam srednia
        }
        if (mean > 3.21 && mean <= 3.80) {
            el.textContent = 3.5; //wstawiam srednia
        }
        if (mean > 3.81 && mean <= 4.20) {
            el.textContent = 4.0; //wstawiam srednia
        }
        if (mean > 4.21 && mean <= 4.80) {
            el.textContent = 4.5; //wstawiam srednia
        }
        if (mean > 4.81 && mean <= 5.00) {
            el.textContent = 5.0; //wstawiam srednia
        }



        //el.textContent = mean; //wstawiam srednia

    }
}

var tab = document.getElementsByTagName("table")[0];

var tr = tab.getElementsByTagName('tr'); //pobieramy wszystkie td z tabeli



var all_grade = new Array();
for (var x = 1; x < tr.length; x++) { //pętla po wszystkich trs

    var oceny = tr[x].getElementsByTagName('td')[2]; //ustawiam sie na ocenach

    var grade = getGrade(oceny.textContent); //zemieniam na stinga

    for (var y = 0; y < grade.length; y++) {
        all_grade.push(grade[y]);
    }

    var mean = average(grade);
    mean = round(mean, 2);

    var el = tr[x].getElementsByTagName('td')[3]; //ustawiam się na drugi element

    el.textContent = mean; //wstawiam srednia

    if (mean <= 2) {
        el.style.backgroundColor = "red";
    }
    if (mean <= 2.81) {
        el.style.backgroundColor = "red";
    } else if (mean >= 4.5) {
        el.style.backgroundColor = "green";
    } else if (mean <= 4.5 && mean >= 2.5) {
        el.style.backgroundColor = "yellow";
    }




}

// WYKRES

all_grade.sort();

function win_or_loss() {
    var zdane = new Array();
    var niezdane = new Array();
    var tab = document.getElementsByTagName("table")[0];

    var tr = tab.getElementsByTagName('tr'); //pobieramy wszystkie td z tabeli

    for (var x = 1; x < tr.length; x++) { //pętla po wszystkich td

        var oceny = tr[x].getElementsByTagName('td')[2]; //ustawiam sie na ocenach

        var grade = getGrade(oceny.textContent); //zemieniam na stinga


        var mean = average(grade);
        if (mean > 2.80) {
            zdane.push(1);

        } else {
            niezdane.push(1);
        }
    }
    var date = new Array();
    date = [zdane.length, niezdane.length];
    return  date;
}


var date = checkAdult(all_grade);

var ctx = document.getElementById("myChart");



var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ["2.0", "2.5", "3.0", "3.5", "4.0", "4.5", "5.0"],
        datasets: [{
                label: 'ilość ocen',
                data: [date[0], date[1], date[2], date[3], date[4], date[5], date[6]],
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',

                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(255, 99, 132, 0.2)'

                ],
                borderColor: [
                    'rgba(255,99,132,1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)',
                    'rgba(255, 159, 64, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
    },
    options: {

        scales: {
            yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
        }
    }
});




var wyniki = win_or_loss();


var cty = document.getElementById('myChartTwo').getContext('2d');
var chart = new Chart(cty, {
    // The type of chart we want to create
    type: 'pie',

    // The data for our dataset
    data: {
        labels: ["Niezdane", "Zdane"],
        datasets: [{
                label: "My First dataset",
                backgroundColor: ['rgba(255, 0, 0,100)',
                    'rgba(0, 255, 0,10)'],
                borderColor: 'rgba(0, 0, 0,100)',
                borderWidth: 1,

                data: [wyniki[1], wyniki[0]]
            }]
    },
    options: {}
});




