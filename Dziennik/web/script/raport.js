function getGrades() {
    var grade = new Array();
    var tab = document.getElementsByTagName("table")[0];
    var tr = tab.getElementsByTagName('tr'); //pobieramy wszystkie td z tabeli


    for (var x = 1; x < tr.length; x++) { //pętla po wszystkich trs
        var oceny = tr[x].getElementsByTagName('td')[5]; //ustawiam sie na ocenach
        grade.push(parseFloat(oceny.textContent));
    }
    return grade;
}
function get_average(grade) {
    var sum = 0;
    var average = 0;
    for (x = 0; x < grade.length; x++) {

        sum += grade[x];
    }
    average = sum / grade.length;
    return average;
}
function generation_pdf() {
    var grade = getGrades();
    var average = String(get_average(grade));

    var h2 = document.getElementsByTagName("h2")[0];
    name = h2.textContent;

    var pdf = new jsPDF();
    pdf.setFontSize(40);
    pdf.text(80, 30, "Raport");
    pdf.setFontSize(18);
    pdf.text(30, 50, "Imie i nazwisko:");
    pdf.text(80, 50, name);
    pdf.text(30, 60, "Srednia:");
    pdf.text(80, 60, average);
    if (get_average(grade) > 4.2) {
        pdf.setFont('helvetica');
        pdf.setFontType('bold');
        pdf.setTextColor(0, 255, 0);
        pdf.text(70, 80, "Przyznano stypednium");
    } else {
        pdf.setFont('helvetica');
        pdf.setFontType('bold');
        pdf.setTextColor(255, 0, 0);
        pdf.text(70, 80, "Nie przyznano stypednium");
    }

    pdf.save('raport.pdf');
}

        