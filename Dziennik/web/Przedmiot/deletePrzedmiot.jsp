<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="przedmiotx" scope="page" class="uz.projekt.db.Przedmiot" />
<jsp:useBean id="connection" scope="session" class="uz.projekt.db.ConnectionManager" />
<jsp:useBean id="auth" scope="session" class="uz.projekt.auth.AuthClass" />
<jsp:setProperty name="auth" property="*" />


<!DOCTYPE html>
<html>
    <head>
        <% if ((auth.authLevel < 2) || (auth.authLevel == null)) response.sendRedirect("/Dziennik/index.jsp"); %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="https://fonts.googleapis.com/css?family=Lato&amp;subset=latin-ext" rel="stylesheet">
        <title>Dziennik - usuwanie przedmiotów</title>
        <link rel="Stylesheet" type="text/css" href="/Dziennik/style/style.css" />
        <link rel="Shortcut icon" href="../src/icon.ico" />
    </head>
    <body style="margin: 0px;">
        <jsp:include page="../admin_menu.jsp" />
        <h1>Usunieto grupe o ID <%=request.getParameter("id")%></h1>
        <%  Integer x = Integer.parseInt(request.getParameter("id"));
            przedmiotx.setId(x);
            connection.createConnection();
            connection.deleteT(przedmiotx);
            connection.closeConnection();
        %>
        <a href="Przedmiot.jsp">HOME</a>

    </body>
</html>
