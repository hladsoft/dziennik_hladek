<%@page import="uz.projekt.db.Wykladowca"%>
<%@page import="uz.projekt.db.Grupa"%>
<%@page import="uz.projekt.db.Przedmiot"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="przedmiot" scope="session" class="uz.projekt.db.Przedmiot" />
<jsp:useBean id="connection" scope="session" class="uz.projekt.db.ConnectionManager" />
<jsp:useBean id="auth" scope="session" class="uz.projekt.auth.AuthClass" />
<jsp:setProperty name="auth" property="*" />

<!DOCTYPE html>
<html>
    <head>
        <% if ((auth.authLevel < 2) || (auth.authLevel == null)) {
                response.sendRedirect("/Dziennik/index.jsp");
            } %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="https://fonts.googleapis.com/css?family=Lato&amp;subset=latin-ext" rel="stylesheet">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.bundle.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
        <link href="https://fonts.googleapis.com/css?family=Lato&amp;subset=latin-ext" rel="stylesheet">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <title>eSES - Przedmioty</title>
        <link rel="Shortcut icon" href="../src/icon.ico" />
        <link rel="Stylesheet" type="text/css" href="/Dziennik/style/style.css" />
    </head>
    <body style="margin: 0px;">
        <jsp:include page="../admin_menu.jsp" />
        <div id="container" style="position: absolute; width: 50%; left: 25%; font-family: 'Lato', sans-serif ">
            <h1 style=" text-align: center; font-family: 'Lato', sans-serif ">Lista przedmiotów</h1>

            <hr/>
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#editModal "style="float: left; margin:10px; 
                    " >Dodaj przedmiot</button> 
            <form action="editPrzedmiot.jsp" id="form1">
                <input class="btn btn-primary" type="submit" value="Edytuj przedmiot" style="float: left; margin:10px; 
                       " />
                <input class="btn btn-primary" type="submit" value="Usuń przedmiot" formaction="deletePrzedmiot.jsp" style="float: left; margin:10px; 
                       " />
            </form>
            <table class="table" border="1">
                <tr>
                    <td><b>check</b></td>
                    <td><b>ID</b></td>
                    <td><b>Nazwa</b></td>
                    <td><b>ID Wykładowcy</b></td>
                </tr>
                <% connection.createConnection();
                    List<Przedmiot> list1 = connection.selectPrzedmiot();
                    while (!list1.isEmpty()) {
                        Przedmiot przedmiotx = list1.get(0);
                %>
                <tr>
                    <td><input type="radio" name="id" value="<%=przedmiotx.getId()%>" form="form1" text="id" required/></td> 
                    <td><%=przedmiotx.getId()%></td>
                    <td><%=przedmiotx.getNazwa()%></td>
                    <td><%=connection.getNameWykladowca(przedmiotx.getIdWykladowcy())%></td>
                </tr>
                <% list1.remove(0);
                    }
                    connection.closeConnection();%>
            </table>
            <a class="btn btn-primary" style="left: 35%; position: absolute;" href="../admin_main.jsp">Strona Główna</a>

            <!-- Modal edit-->
            <div class="modal fade" id="editModal" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-body" style="text-align: center;">
                            <h1>Dodaj przedmiot:</h1>
                            <form action="addedP.jsp" method="POST">
                                <input type="text" name="nazwa" placeholder="Wpisz nazwę przedmiotu" required pattern="[ A-Za-z]{3,}" />
                                <select name="idWykladowcy">
                                    <% connection.createConnection();
                                        List<Wykladowca> list2 = connection.selectWykladowcy();
                                        while (!list2.isEmpty()) {
                                            Wykladowca wykladowca = list2.get(0);
                                    %>
                                    <option value="<%=wykladowca.getIdWykladowca()%>"><%=wykladowca.getImie()%> <%=wykladowca.getNazwisko()%></option>
                                    <% list2.remove(0);
                                        }
                                        connection.closeConnection();%>
                                    </table>
                                </select>
                                <div>
                                    <h1></h1>
                                    <input style="width: 150px;" class="btn btn-primary" type="submit" value="Dodaj" />
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">

                            <button type="button" class="btn btn-default" data-dismiss="modal">Anuluj</button>

                        </div>
                    </div>
                </div>
            </div>




        </div>
    </body>
</html>
