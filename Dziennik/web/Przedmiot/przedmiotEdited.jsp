<%-- 
    Document   : studentEdited
    Created on : 2017-05-08, 15:55:39
    Author     : Rain
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page errorPage = "../ShowError.jsp" %>
<jsp:useBean id="connection" scope="session" class="uz.projekt.db.ConnectionManager" />
<jsp:useBean id="przedmiotx" scope="page" class="uz.projekt.db.Przedmiot" />
<jsp:setProperty name="przedmiotx" property="*" />
<jsp:useBean id="auth" scope="session" class="uz.projekt.auth.AuthClass" />
<jsp:setProperty name="auth" property="*" />
<!DOCTYPE html>
<html>
    <head>
        <% if ((auth.authLevel < 2) || (auth.authLevel == null)) response.sendRedirect("/Dziennik/index.jsp"); %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Dziennik - dodano przemiot <%=przedmiotx.getId()%></title>
        <link rel="Stylesheet" type="text/css" href="/Dziennik/style/style.css" />
        <link href="https://fonts.googleapis.com/css?family=Lato&amp;subset=latin-ext" rel="stylesheet">
        <link rel="Shortcut icon" href="../src/icon.ico" />
    </head>
    <body>
        <jsp:include page="../admin_menu.jsp" />
        <h1>Przedmiot  <%=przedmiotx.getId()%> został zmieniony!</h1>
        <%  Integer x = Integer.parseInt(request.getParameter("id"));
            przedmiotx.setId(x);
            connection.createConnection();
            connection.editT(przedmiotx);
            connection.closeConnection();
        %>
        <a href="Przedmiot.jsp">HOME</a>
    </body>
</html>
