<%-- 
    Document   : addedS
    Created on : 2017-05-05, 18:34:17
    Author     : Rain
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page errorPage = "../ShowError.jsp" %>
<jsp:useBean id="przedmiot" scope="session" class="uz.projekt.db.Przedmiot" />
<jsp:setProperty name="przedmiot" property="*" />
<jsp:useBean id="connection" scope="session" class="uz.projekt.db.ConnectionManager" />
<jsp:useBean id="auth" scope="session" class="uz.projekt.auth.AuthClass" />
<jsp:setProperty name="auth" property="*" />
<!DOCTYPE html>

<html>
    <head>
        <link rel="Stylesheet" type="text/css" href="/Dziennik/style/style.css" />
        <link href="https://fonts.googleapis.com/css?family=Lato&amp;subset=latin-ext" rel="stylesheet">
        <% if ((auth.authLevel < 2) || (auth.authLevel == null)) response.sendRedirect("/Dziennik/index.jsp"); %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Dziennik - Dodano ocene</title>
        <link rel="Shortcut icon" href="../src/icon.ico" />
    </head>
    <body>
        <jsp:include page="../admin_menu.jsp" />
        <% connection.createConnection();
            connection.addT(przedmiot);
            connection.closeConnection();%>
        <h1>Przedmiot <%=przedmiot.getNazwa()%> dodany !</h1>
        <a href="Przedmiot.jsp">home</a>
    </body>
</html>
