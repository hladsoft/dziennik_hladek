<%@page import="java.util.List"%>
<%@page import="uz.projekt.db.Wykladowca"%>
<%@page import="uz.projekt.db.Grupa"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="przedmiot" scope="session" class="uz.projekt.db.Przedmiot" />
<jsp:useBean id="connection" scope="session" class="uz.projekt.db.ConnectionManager" />
<jsp:useBean id="auth" scope="session" class="uz.projekt.auth.AuthClass" />
<jsp:setProperty name="auth" property="*" />

<!DOCTYPE html>
<html>
    <head>
        <% if ((auth.authLevel < 2) || (auth.authLevel == null)) response.sendRedirect("/Dziennik/index.jsp"); %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Dziennik - Dodawanie oceny</title>
        <link rel="Shortcut icon" href="../src/icon.ico" />
        <link href="https://fonts.googleapis.com/css?family=Lato&amp;subset=latin-ext" rel="stylesheet">
        <link rel="Stylesheet" type="text/css" href="/Dziennik/style/style.css" />
    </head>
    <body style="margin: 0px;">
        <jsp:include page="../admin_menu.jsp" />
        <h1>Dodaj przedmiot:</h1>
        <form action="addedP.jsp" method="POST">
            <input type="text" name="nazwa" placeholder="wpisz nazwe" required pattern="[ A-Za-z]{3,}" />
            <select name="idWykladowcy">
                <% connection.createConnection();
                    List<Wykladowca> list = connection.selectWykladowcy();
                    while (!list.isEmpty()) {
                        Wykladowca wykladowca = list.get(0);
                %>
                <option value="<%=wykladowca.getIdWykladowca()%>"><%=wykladowca.getImie()%> <%=wykladowca.getNazwisko()%></option>
                <% list.remove(0);
                }
                connection.closeConnection();%>
                </table>
            </select>
            <input type="submit" value="add" />
        </form>
    </body>
</html>
