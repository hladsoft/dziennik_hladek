<%-- 
    Document   : editStudent
    Created on : 2017-05-08, 15:47:01
    Author     : Rain
--%>
<%@page import="java.util.List"%>
<%@page import="uz.projekt.db.Wykladowca"%>
<%@page import="uz.projekt.db.Wykladowca"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="przedmiotx" scope="page" class="uz.projekt.db.Przedmiot" />
<jsp:useBean id="connection" scope="session" class="uz.projekt.db.ConnectionManager" />
<jsp:useBean id="auth" scope="session" class="uz.projekt.auth.AuthClass" />
<jsp:setProperty name="auth" property="*" />

<!DOCTYPE html>
<html>
    <head>
        <% if ((auth.authLevel < 2) || (auth.authLevel == null)) response.sendRedirect("/Dziennik/index.jsp"); %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Dziennik - Edycja przedmiotów</title>
        <link href="https://fonts.googleapis.com/css?family=Lato&amp;subset=latin-ext" rel="stylesheet">
        <link rel="Stylesheet" type="text/css" href="/Dziennik/style/style.css" />
        <link rel="Shortcut icon" href="../src/icon.ico" />
    </head>
    <body style="margin: 0px;">
        <jsp:include page="../admin_menu.jsp" />
        <%  Integer x = Integer.parseInt(request.getParameter("id"));
            przedmiotx.setId(x);
            connection.createConnection();
            connection.readOneT(przedmiotx);
            connection.closeConnection();
        %>
        <h1>Hello World!</h1>
        <form action="przedmiotEdited.jsp" method="POST">
            <input type="text" name="id" value="<%=przedmiotx.getId()%>" readonly="readonly" />
            <input type="text" name="nazwa" value="<%=przedmiotx.getNazwa()%>" required pattern="[ A-Za-z]{3,}" />
            <select name="idWykladowcy">
                <% connection.createConnection();
                    List<Wykladowca> list = connection.selectWykladowcy();
                    while (!list.isEmpty()) {
                        Wykladowca wykladowca = list.get(0);
                %>
                <option value="<%=wykladowca.getIdWykladowca()%>"><%=wykladowca.getImie()%> <%=wykladowca.getNazwisko()%></option>
                <% list.remove(0);
                }
                connection.closeConnection();%>
                <input type="submit" value="Edytuj!" />
        </form>
    </body>
</html>
