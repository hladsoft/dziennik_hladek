<%-- 
    Document   : index
    Created on : 2017-05-05, 18:04:25
    Author     : Rain

Ta strona ma wygladac jak w specyfikacji to co nam dali, to bedzie glowna strona do wykonywania operacji wszelakich
--%>

<%@page import="uz.projekt.db.Student"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="connection" scope="session" class="uz.projekt.db.ConnectionManager" />
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.bundle.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link href="https://fonts.googleapis.com/css?family=Lato&amp;subset=latin-ext" rel="stylesheet">
        <link rel="Stylesheet" type="text/css" href="/Dziennik/style/style.css" />

        <title>eSES - Studenci</title>
        <script>
            $(document).ready(function () {
                $rows = $('#tabela tbody tr');
                $('#szukaj').keyup(function () {

                    var val = '^(?=.*\\b' + $.trim($(this).val()).split(/\s+/).join('\\b)(?=.*\\b') + ').*$',
                            reg = RegExp(val, 'i'),
                            text;

                    $rows.show().filter(function () {
                        text = $(this).text().replace(/\s+/g, ' ');
                        return !reg.test(text);
                    }).hide();
                });
            });
        </script>  

    </head>
    <body>

        
        <div id="container" style="position: absolute; width: 50%; left: 25%; font-family: 'Lato', sans-serif ">
            <h1 style=" text-align: center; font-family: 'Lato', sans-serif ">Studenci</h1> 
            <hr/>
            <form action="ocena/showGrades.jsp" id="form1">
                <input class="btn btn-primary" type="submit" value="Pokaz oceny" />
            </form>
            <input class="form-control" type="text" name="szukaj" placeholder="szukaj" id="szukaj"/>
            <table class="table" id="tabela" border="1" >    
                <thead>
                    <tr>
                        <td><b>check</b></td>
                        <td><b>ID</b></td>
                        <td><b>Pesel</b></td>
                        <td><b>Nr indeksu</b></td>
                        <td><b>Imię</b></td>
                        <td><b>Nazwisko</b></td>
                        <td><b>Rok studiow</b></td>
                        <td><b>Grupa</b></td>

                    </tr>
                </thead>
                <% connection.createConnection();
                    List<Student> list = connection.selectStudents();
                    while (!list.isEmpty()) {
                        Student student = list.get(0);
                %>
                <tbody>
                    <tr>

                        <td><input type="radio" name="id" value="<%=student.getId()%>" form="form1" text="id" required/></td> 
                        <td><%=student.getId()%></td>
                        <td><%=student.getPesel()%></td>
                        <td><%=student.getNrIndeksu()%></td>
                        <td><%=student.getImie()%></td>
                        <td><%=student.getNazwisko()%></td>
                        <td><%=student.getRokStudiow()%></td>
                        <td><%=connection.getNameGroup(student.getIdGrupa())%></td>
                    </tr>

                    <% list.remove(0);
                        }
                        connection.closeConnection();%>
                </tbody>
            </table>
            <a class="btn btn-primary" style="left: 40%; position: absolute;" href="../user_main.jsp">Strona Główna</a>
        </div>
    </body>
</html>
