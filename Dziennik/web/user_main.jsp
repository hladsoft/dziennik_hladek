<%-- 
    Document   : index
    Created on : 2017-05-05, 18:04:25
    Author     : Rain
--%>

<%@page import="uz.projekt.db.Student"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.bundle.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Menu użytkownika</title>
    </head>
    <body>
        <div id="container" style=" width: 50%;  font-family: 'Lato', sans-serif ">
            <div class="well" style="position: absolute; 
                 top: 30%;
                left: 25%;
                 margin: 20px;
                 width: 50%;
                 ">
                <form action="student_user.jsp">
                    <input class="btn btn-primary" style="width: 100%;margin:10px; 
padding:10px;" type="submit" value="Oceny studentow" />
                </form>
                <form action="przedmiot_user.jsp">
                    <input class="btn btn-primary"  style="width: 100%;margin:10px; 
padding:10px;;" type="submit" value="Oceny przedmioty" />
                </form>
                <a class="btn btn-danger"  style="width: 100%;margin:10px; 
padding:10px;" href="index.jsp">Wyloguj</a>
            </div>
        </div>

    </body>
</html>
